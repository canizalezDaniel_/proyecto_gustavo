<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>FACTURA</title>
    <style>
        .fecha_factura{
            margin-top: 150px;
            margin-left: 400px;
        }

        .membrete_factura{
            margin-top: 100px;
        }


        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }



    </style>
</head>
<body>

@foreach($factura as $item)
<div class="fecha_factura">
    <table >
        <thead>

        </thead>
        <tbody>
        <tr>
            <td> Nº Factura </td>
            <td> {{$item->factura_codigo}}</td>

        </tr>
        <tr>
            <td> Fecha de Factura</td>
            <td> <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $item->fecha_emision)->formatLocalized('%d/%m/%Y'); ?></td>

        </tr>
        <tr>
            <td> Fecha de vencimiento</td>
            <td> <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $item->vencimiento)->formatLocalized('%d/%m/%Y'); ?></td>

        </tr>

        </tbody>
    </table>
</div>

<div id="nota_info">
    <h2> {{$item->nombre_razon_social}} </h2>
    <div>{{$item->direccion_fiscal}}. </div>
    <div>{{$item->ci_rif}}</div>
    <div>{{$item->telefono}}</div>
    <h3>Vendedor: {{$item->nombre}}   </h3>
</div>

@endforeach
<br/><br/>
<table>

    <tr>

        <th width="1%">
            <b>CANT.</b>
        </th>
        <th width="35%">
            <b>PRODUCTO</b>
        </th>
        <th >
            <b>PRECIO UNIT(Bs)</b>
        </th>
        <th >
            <b>MONTO Bs</b>
        </th>
    </tr>

    <tbody>

    @foreach($items_factura as $items)
        <tr>

            <td>
                {{$items->cantidad}}
            </td>
            <td>
                {{mb_strtoupper($items->descripcion)}}
            </td>

            <td align="right">
                {{$items->precio_unitario_bs}}
            </td>
            <td align="right">
                {{$items->total_item_bs}}
            </td>

        </tr>
    @endforeach

    @foreach($factura as $item)
        <tr>
            <td colspan="4" ></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: right"><b>Total base imponible Bs.</b></td>
            <td style="text-align: left">
                {{$item->total_base}}
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: right"><b>Ajustes Bs.</b></td>
            <td style="text-align: left">
                {{$item->ajustes}}
            </td>
        </tr>

        <tr>
            <td colspan="3" style="text-align: right"><b>IVA Bs.</b></td>
            <td style="text-align: left">
                {{$item->iva_bs}}
            </td>
        </tr>

        <tr>
            <td colspan="3" style="text-align: right"><b>Total Bs.</b></td>
            <td style="text-align: left">
                {{$item->total_bs}}
            </td>
        </tr>
    @endforeach
    <br>

    </tbody>

</table>




</body>
</html>