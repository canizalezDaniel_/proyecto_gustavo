<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>NOTA DE ENTREGA</title>
    <link href="dist/css/pdf.css" rel="stylesheet" type="text/css">

</head>
<body>




    @foreach($nota_entrega as $nota)
    <div id="nota_info">
        <h2>NOTA DE ENTREGA # {{$nota->id}} </h2>
        <div><b>Lugar de Emisión: </b>{{$nota->lugar_emision}} </div>
        <div><b>Fecha de emisión:</b>{{$nota->fecha_emision}}</div>
    </div>
    <br/><br/><br/><br/><br/>
    <div id="details" class="clearfix">
        <div id="client">


            <h3>{{$nota->nombre_razon_social}}</h3>
            <div class="date">RIF: {{$nota->ci_rif}}</div>
            <div class="date">Teléfono: {{$nota->telefono}}</div>
            <div class="date">{{$nota->direccion_fiscal}}</div>

        </div>
    </div>
    @endforeach
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>

            <th>ITEM</th>
            <th>CÓDIGO</th>
            <th >PRODUCTO</th>
            <th>CANTIDAD SOLICITADA</th>

        </tr>
        </thead>
        <tbody>
        @foreach($items_notas_entregas as $items)
        <tr>
            <td class="no">{{$items->id}}</td>
            <td>{{$items->codigo}}</td>
            <td class="desc">{{ $items->descripcion }}</td>
            <td class="unit">{{ $items->cantidad }}</td>

        </tr>
        @endforeach

        </tbody>

    </table>


</body>
</html>