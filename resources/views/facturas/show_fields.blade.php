<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $factura->id !!}</p>
</div>

<!-- Razon Social Field -->
<div class="form-group">
    {!! Form::label('razon_social', 'Razon Social:') !!}
    <p>{!! $factura->razon_social !!}</p>
</div>

<!-- Direccion Fiscal Field -->
<div class="form-group">
    {!! Form::label('direccion_fiscal', 'Direccion Fiscal:') !!}
    <p>{!! $factura->direccion_fiscal !!}</p>
</div>

<!-- Ci Rif Field -->
<div class="form-group">
    {!! Form::label('ci_rif', 'Ci Rif:') !!}
    <p>{!! $factura->ci_rif !!}</p>
</div>

<!-- Fecha Emision Field -->
<div class="form-group">
    {!! Form::label('fecha_emision', 'Fecha Emision:') !!}
    <p>{!! $factura->fecha_emision !!}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Telefono:') !!}
    <p>{!! $factura->telefono !!}</p>
</div>

<!-- Nota Entrega Codigo Field -->
<div class="form-group">
    {!! Form::label('nota_entrega_codigo', 'Nota Entrega Codigo:') !!}
    <p>{!! $factura->nota_entrega_codigo !!}</p>
</div>

<!-- Condicion Pago Field -->
<div class="form-group">
    {!! Form::label('condicion_pago', 'Condicion Pago:') !!}
    <p>{!! $factura->condicion_pago !!}</p>
</div>

<!-- Dias Field -->
<div class="form-group">
    {!! Form::label('dias', 'Dias:') !!}
    <p>{!! $factura->dias !!}</p>
</div>

<!-- Forma Pago Field -->
<div class="form-group">
    {!! Form::label('forma_pago', 'Forma Pago:') !!}
    <p>{!! $factura->forma_pago !!}</p>
</div>

<!-- Numero Cheque Field -->
<div class="form-group">
    {!! Form::label('numero_cheque', 'Numero Cheque:') !!}
    <p>{!! $factura->numero_cheque !!}</p>
</div>

<!-- Banco Field -->
<div class="form-group">
    {!! Form::label('banco', 'Banco:') !!}
    <p>{!! $factura->banco !!}</p>
</div>

<!-- Credito Dias Field -->
<div class="form-group">
    {!! Form::label('credito_dias', 'Credito Dias:') !!}
    <p>{!! $factura->credito_dias !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $factura->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $factura->updated_at !!}</p>
</div>

