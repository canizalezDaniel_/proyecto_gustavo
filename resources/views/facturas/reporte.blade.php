 @extends('layouts.admin')

@section('content')

@include('flash::message')

<div class="box">
<section class="content-header">
      <h1>
        Reportes
        <small>Factura</small>
      </h1>
 </section>

            <div class="box-header">


            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="example4" class="table table-bordered table-hover dataTable" role="grid">
                <thead>
                  <th>CODÍGO FACTURA</th>
                  <th>
                    ESTATUS
                  </th>
                  <th>EMPRESA</th>
                  <th>RIF/CI</th>
                  <th>
                    NOTA DE ENTREGA
                  </th>
                  <th>FECHA EMISIÓN</th>
                  <th>ACCIONES</th>
                </thead>
                <tbody>
                  @foreach($facturas as $factura)
                    <tr>
                      <td><strong>{{$factura->factura_codigo}}</strong></td>
                      <td>
                        @if($factura->estatus==1)
                          <span class="label label-success">ACTIVA</span>
                        @else
                          <span class="label label-warning"> ANULADA </span>
                        @endif
                      </td>
                      <td>{{$factura->nombre_razon_social}}</td>
                      <td>{{$factura->ci_rif}}</td>
                      <td>
                        @if($factura->nota_codigo=="")
                            <span class="label label-warning">Sin nota de entrega</span>
                        @else
                            <span class="label label-danger">{{$factura->nota_codigo}}</span>
                        @endif

                      </td>
                      <td>
                          <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $factura->fecha_emision)->formatLocalized('%d %b %Y'); ?>
                      </td>
                      <td>
                        {!! Form::open(['route' => ['facturas.destroy', $factura->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="impresoraPdffactura/{{$factura->id}}" target="_blank" class='btn btn-default btn-sm'><i class="fa fa-print" aria-hidden="true"></i></a>
                          <a href="{!! route('facturas.edit', [$factura->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                          <a href="anularFactura/{{$factura->id}}" class='btn btn-warning btn-sm' onclick="return confirm('¿Desea anular el registro seleccionado ?')"><i class="fa fa-ban" aria-hidden="true"></i></a>

                          {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Desea eliminar el registro seleccionado ?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="col-xs-12">
                {{-- Página {{$facturas->currentPage()}}  de {{ $facturas->lastPage()}}
                 --}}<ul class="pagination hidden-xs pull-right">
                  {{ $facturas->links()
                       
                    }}

                </ul>
              </div>
            </div>
</div>       

@endsection