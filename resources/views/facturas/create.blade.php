@extends('layouts.admin')

@section('content')

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'facturas.store', 'name'=>'formulario', 'novalidate']) !!}

            @include('facturas.fields')

        {!! Form::close() !!}
    </div>
@endsection
