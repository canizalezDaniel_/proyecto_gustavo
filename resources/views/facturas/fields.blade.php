<!-- Main content -->
<section class="invoice">
  <!-- title row -->

  @if(Session::get('mensaje'))
  <div class="alert alert-warning">
  <?php echo Session::get('mensaje'); ?>
  </div>
  @endif
  <div class="row">
  <div class="col-xs-12">
   <a class="btn btn-default btn-sm" href="javascript:history.go(-1)"><label class="fa fa-undo"></label> Atras!</a>
      <h2>Crear factura
          <small># {{$factura_codigo}}</small>

      </h2>
   </div>
  </div>
  <br>
  <!-- info row -->
  <div class="row invoice-info">
    @include('layouts.cabecera_factura')


    <div class="col-sm-3">
      <div class="form-group" ng-class="{ 'has-error' : formulario.factura_codigo.$invalid && !formulario.factura_codigo.$pristine && formulario.factura_codigo.$error.maxlength}">

      <label for="">FACTURA Nº</label>
      {!! Form::text('factura_codigo', $factura_codigo, ['class' => 'form-control input-sm', 'placeholder'=>'Codigo factura',  'required', 'readonly']) !!}
      </div>
    </div>

    <div class="col-xs-3">
      <label for="">NOTA DE ENTREGA Nº</label>
      <select class="form-control input-sm" name="nota_codigo" id="empresa">
        <option value="">Sin nota de entrega</option>
        @foreach($notas_entregas as $nota)
          <option value="{{$nota->nota_codigo}}">{{$nota->nota_codigo}}</option>
        @endforeach
      </select>
    </div>






  </div>

  <h2></h2>
  <!-- Table row -->
  <div class="row" ng-app="miApp" ng-controller="CartForm" ng-init="iva='{{$iva}}'; tasa_cambio='{{$tasa_cambio}}'; iva_bs_porcentaje='{{$iva_bs_porcentaje}}'">

    <div class="col-xs-12 table-responsive">
      <a href ng-click="addItem()" class="btn btn-danger pull-left"><i class="fa fa-plus-circle"> </i> <span> Agregar Item</span></a>

      @include('layouts.tabla_factura')

    </div>
    <!-- /.col -->
    <div class="col-xs-12">
      <h2 class="page-header">
        Forma de pago:

      </h2>
    </div>
    <div class="col-md-2">


      <div class="checkbox">
        <label>
          {!! Form::radio('forma_pago', "Efectivo") !!}  Efectivo
        </label>
      </div>
      <div class="checkbox">
        <label>
          {!! Form::radio('forma_pago', "Cheque") !!} Cheque
        </label>
      </div>
      {{--<div class="checkbox">
        <label>
          {!! Form::checkbox('forma_pago', "TDC") !!}Tarjeta de crébito
        </label>
      </div>--}}

    </div>
    <div class="col-md-2">
      <label for="">Crédito días</label>
      <div class="input-group">
        <span class="input-group-addon">
          {{-- {!! Form::checkbox('forma_pago', "Credito", ['class' => 'form-control input-sm', 'ng-model'=>'forma_pago2']) !!} --}}
          <input name="forma_pago" type="checkbox" value="Credito" ng-model="forma_pago2">

        </span>
        {!! Form::text('credito_dias', null, ['class' => 'form-control input-sm', 'ng-disabled'=>"!forma_pago2", 'data-inputmask'=>"'mask': '9','repeat': 4, 'greedy': false"]) !!}
      </div>


    </div>

    <div class="col-md-2">
      <label for="">Cheque</label>
      <div class="input-group">
        <span class="input-group-addon">
          <input name="forma_pago" type="checkbox" value="Cheque" ng-model="forma_pago3">
        </span>
        {!! Form::text('cheque_numero', null, ['class' => 'form-control input-sm', 'ng-disabled'=>"!forma_pago3", 'data-inputmask'=>"'mask': '9', 'repeat': 11, 'greedy': false"]) !!}
      </div>
    </div>
    <div class="col-md-2">
      <label for="">Banco</label>
      <div class="input-group">
        <span class="input-group-addon">
          {{-- {!! Form::radio('forma_pago', "Banco", ['class' => 'form-control input-sm']) !!} --}}
          <input name="forma_pago" type="checkbox" value="Banco" ng-model="forma_pago4">

        </span>
        {!! Form::text('banco_cuenta', null, ['class' => 'form-control input-sm', 'ng-disabled'=>"!forma_pago4", 'data-inputmask'=>"'mask': '(9999)-9999-99-9999999999', 'greedy': false"]) !!}
      </div>
    </div>


  </div>

  <br>
  <div class="row no-print">
    <div class="col-xs-6">
      {{ Form::button('<i class="fa fa-save"><span> Guardar </span> </i>', ['type' => 'submit', 'class' => 'btn btn-success btn-lg', 'ng-disabled'=>'formulario.$invalid'] )  }}
      <a href="{{URL::to('home')}}" class="btn btn-danger  btn-lg"> <i class="fa fa-undo"></i> <span>Cancelar</span></a>

    </div>
  </div>
</section>
<!-- /.content -->


<!-- /.content-wrapper -->
