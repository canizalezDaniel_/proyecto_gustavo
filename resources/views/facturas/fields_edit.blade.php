<!-- Main content -->
<section class="invoice">
  <!-- title row -->

  @if(Session::get('mensaje'))
  <div class="alert alert-warning">
  <?php echo Session::get('mensaje'); ?>
  </div>
  @endif
  <div class="row">

    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    
  {{-- Cabecera --}}
    
    <div class="col-sm-6 invoice-col">

      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Lugar de Emisión</label> --}}
        <label for="">Lugar de emisión:</label>
        {!! Form::text('lugar_emision', null, ['class' => 'form-control input-sm']) !!}
        {{-- <p ng-show="formulario.lugar_emision.$invalid && !formulario.lugar_emision.$pristine" class="help-block">Lugar de emisión requerido.</p> --}}

      </div>
      <div class="form-group">
        <label for="">Empresa:</label>
        @foreach($cliente_factura as $cliente)
        {!! Form::text('cliente', $cliente->nombre_razon_social, ['class' => 'form-control input-sm', 'READONLY']) !!}
      {!! Form::hidden('empresa_nombre',  $cliente->id, ['class' => 'form-control input-sm', 'READONLY']) !!}
    
      @endforeach
        
      </div>


    </div>

    <!-- /.col -->
    <div class="col-sm-3 invoice-col">
      {{-- Formulario 2 --}}

      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Fecha emisión:</label>
        {!! Form::text('fecha_emision', Carbon\Carbon::today()->toDateString(), ['class' => 'form-control input-sm', 'placeholder'=>'Fecha', 'id'=> 'datepicker']) !!}
        {{-- <p ng-show="formulario.fecha_emision.$invalid && !formulario.fecha_emision.$pristine" class="help-block">Fecha requerida.</p> --}}

      </div>


    </div>

    <div class="col-sm-3 invoice-col">
      {{-- Formulario 3 --}}

      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Telefono:</label>
        {!! Form::text('telefono', null, ['class' => 'form-control input-sm', 'placeholder'=>'Numero de telefono', 'READONLY']) !!}



      </div>


    </div>

    <div class="col-sm-6 invoice-col">
      {{-- Formulario 4 --}}

      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">RIF/C.I.:</label>
        {!! Form::text('ci_rif', null, ['class' => 'form-control input-sm', 'placeholder'=>'RIF/CI', 'READONLY']) !!}

      </div>


    </div>


    <!-- /.col -->
    <div class="col-sm-12 invoice-col">
      {{-- Formulario 3 --}}
      <div class="form-group">
        {{-- <label for="exampleInputPassword1">Password</label> --}}
        <label for="">Domicilió fiscal:</label>
        {!! Form::textarea('empresa_direccion', null, ['class' => 'form-control input-sm', 'placeholder'=>'Domicilió Fiscal:', 'rows'=>'2', 'cols'=>'4','READONLY']) !!}

      </div>
    </div>


    <div class="col-sm-6">
      {{-- Formulario 3 --}}

      <div class="form-group" >
        {{-- <label for="exampleInputPassword1">Password</label> --}}

        <div class="col-md-2">
          <label for="">CONTADO</label>
          <div class="input-group input-group-sm">
            @if ($factura->condicion_pago=="Contado")
              {!! Form::checkbox('condicion_pago', "Contado", true) !!}
            @else
              {!! Form::checkbox('condicion_pago', "Contado", false) !!}
            @endif
            

          </div>
        </div>
        <div class="col-md-4">
          <label for="">CRÉDITO</label>
          <div class="input-group nput-group-sm">

             @if ($factura->condicion_pago=="Credito")
            <span class="input-group-addon">
              

              <input name="condicion_pago" type="checkbox" value="Credito" checked="checked">

            </span>
            {!! Form::text('credito_condicion_dias', null, ['class' => 'form-control input-sm', 'placeholder'=> 'Días','data-inputmask'=>"'mask': '9', 'repeat': 11, 'greedy': false"]) !!}
            @else
              <span class="input-group-addon">
              

              <input name="condicion_pago" type="checkbox" value="Credito" ng-model="condicion_pago2">

            </span>
            {!! Form::text('credito_condicion_dias', null, ['class' => 'form-control input-sm', 'placeholder'=> 'Días', 'ng-disabled'=>"!condicion_pago2", 'data-inputmask'=>"'mask': '9', 'repeat': 11, 'greedy': false"]) !!}
            @endif
          </div>
        </div>
        <div class="col-md-3" ng-class="{ 'has-error' : formulario.vencimiento.$invalid && !formulario.vencimiento.$pristine }">
          <label for="">VENCIMIENTO</label>
          <div class="input-group input-group-sm" >

            {!! Form::text('vencimiento', null, ['class' => 'form-control input-sm', 'id'=> 'datepicker2']) !!}

          </div>
        </div>



      </div>
    </div>



  {{-- Fin cabecera --}}

    <div class="col-sm-3">
      <div class="form-group" ng-class="{ 'has-error' : formulario.factura_codigo.$invalid && !formulario.factura_codigo.$pristine && formulario.factura_codigo.$error.maxlength}">

      <label for="">FACTURA Nº</label>
      {!! Form::text('factura_codigo', $factura_codigo, ['class' => 'form-control input-sm', 'placeholder'=>'Codigo factura',  'required']) !!}
      </div>
    </div>

    <div class="col-xs-3">
      <label for="">NOTA DE ENTREGA Nº</label>
      
      {!! Form::text('nota_codigo', null, ['class' => 'form-control input-sm', 'placeholder'=>'Nota codigo',  'required', 'readonly']) !!}


    </div>






  </div>

  <h2></h2>
  <!-- Table row -->
  <div class="row">

    <div class="col-xs-12 table-responsive">
      

      {{-- Items factura --}}

      
  
     <table class="table table-striped">
  <thead>
    <tr>
     <th class="col-sm-0">RENGLÓN</th>
      <th class="col-sm-0">CANTIDAD</th>
      <th class="col-sm-3">DESCRIPCIÓN</th>
      <th class="col-sm-2">PRECIO UNITARIO($)</th>
      <th class="col-sm-2">TOTAL $</th>
      <th class="col-sm-2">PRECIO UNITARIO(Bs)</th>
      <th class="col-sm-2">TOTAL Bs</th>
      

    </tr>
  </thead>
  <tbody>
  @foreach ($items_facturas as $items)
    {{-- expr --}}

    <tr>
     <td>
        {!! Form::hidden('id[]', $items->id, ['class' => 'form-control input-sm']) !!}
        {!! Form::number('renglon[]', $items->renglon, ['class' => 'form-control input-sm']) !!}
         
          

      </td>
      <td>
        
        {!! Form::number('cantidad[]', $items->cantidad, ['class' => 'form-control input-sm', 'readonly']) !!}
         
          

      </td>
      <td>
        
        
        {!! Form::textarea('descripcion[]', $items->descripcion, ['class'=>'form-control', 'size'=>'10x4']) !!}

      </td>
      <td>

        
        {!! Form::text('precio_unitario_usd[]',$items->precio_unitario_usd, ['class' => 'form-control', 'decimal-min'=>'0', 'input-decimal-separator'=>'3', 'readonly']) !!}

      </td>
      <td>
        <div class="input-group">
       
        {!! Form::text('total_item_usd[]', $items->total_item_usd, ['class' => 'form-control','step'=>'any', 'readonly', 'readonly']) !!}

       
      </div>
      </td>

      <td>

        

        {!! Form::text('precio_unitario_bs[]',  $items->precio_unitario_bs, ['class' => 'form-control','decimal-min'=>'0', 'input-decimal-separator'=>'3', 'readonly']) !!}
     
     


      </td>
      <td>

      

        {!! Form::text('total_item_bs[]', $items->total_item_bs, ['class' => 'form-control','step'=>'any', 'readonly']) !!}
      


      </td>
     


    </tr>

    @endforeach

    <tr>
      <td> </td>
      <td> </td>
      <td  class="text-right"> SUB-TOTAL</td>
      <td>
      {!! Form::text('sub_total_usd', $factura->sub_total_usd, ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td> </td>

    </tr>
    <tr>
      <td> </td>
      <td> </td>
      <td  class="text-right"> IVA USD {{number_format($iva*100)}} %</td>
      <td>  <input type="hidden" ng-model="iva">
      {!! Form::text('iva_usd', $factura->iva_usd, ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td> </td>

    </tr>

    <tr>
      <td> </td>
        <td> </td>
      <td  class="text-right"> TOTAL USD</td>
      <td> {!! Form::text('total_usd', $factura->total_usd, ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td> </td>

    </tr>

    <tr >
      <td colspan="5">
          <strong>NOTA: MONTO CALCULADO A UNA TASA DE CAMBIO DE {{$tasa_cambio}} Bs./$</strong>
         <textarea rows="8" cols="120" name="observaciones" id="T2" style="font-size: 10px">
           {!! $factura->observaciones !!}
         </textarea>
      </td>

       <td></td>
       <td></td>

    </tr>
    <tr >
    <td></td>
      <td></td>
       <td></td>
        <td></td>
         <td></td>
      <td  class="text-right">Total Base imponible Bs</td>
      
      
      
      <td > 

      {!! Form::text('total_base', $factura->total_base, ['class' => 'form-control input-sm', 'readonly']) !!}

      </td>

     

      <td></td>

    </tr>
    <tr>
      <td colspan="5"></td>


      <td  class="text-right">Ajustes</td>
      <td> {!! Form::number('ajustes', $factura->ajustes, ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td></td>

    </tr>

    <tr>
    <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td  class="text-right">Total Base imponible Bs</td>
      
      <td>
      {!! Form::text('total_base_ajustes', $factura->total_base_ajustes , ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      

      <td></td>

    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td  class="text-right">IVA {{number_format($iva_bs_porcentaje*100)}}% Bs</td>

      <td >
        {!! Form::text('iva_bs', $factura->iva_bs, ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      
      <td></td>

    </tr>
    <tr>
    <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td  class="text-right">Total a pagar</td>
      
      <td>
        {!! Form::text('total_bs', $factura->total_bs, ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>

      
      <td></td>

    </tr>




  </tbody>
</table>


      {{-- Fin Items factura --}}

    </div>
    <!-- /.col -->
    <div class="col-xs-12">
      <h2 class="page-header">
        Forma de pago:

      </h2>
    </div>
    <div class="col-md-2">


      <div class="checkbox">
       

        @if ($factura->forma_pago == "Efectivo")
        <label>
          {!! Form::checkbox('forma_pago', "Efectivo", true) !!}  Efectivo
       </label>
        @else
        <label>
          {!! Form::checkbox('forma_pago', "Efectivo", false) !!}  Efectivo
        </label>
        @endif
          

          

      
      </div>
      <div class="checkbox">
        
          @if ($factura->forma_pago == "TDD")
          <label>
          {!! Form::checkbox('forma_pago', "TDD", true) !!}Tarjeta de débito
          </label>
          @else
           <label>
           {!! Form::checkbox('forma_pago', "TDD", false) !!}Tarjeta de débito
           </label>
        @endif
          
      
      </div>
      <div class="checkbox">
        @if ($factura->forma_pago  =="TDC")
        <label>
          {!! Form::checkbox('forma_pago', "TDC", true) !!}Tarjeta de crébito
        </label>
        @else
        <label>
          {!! Form::checkbox('forma_pago', "TDC", false) !!}Tarjeta de crébito
        </label>
        @endif
      </div>

    </div>
    <div class="col-md-2">
      <label for="">Crédito días</label>
      <div class="input-group">
          @if ($factura->forma_pago  =="Credito")
            <span class="input-group-addon">

            <input name="forma_pago" type="checkbox" value="Credito" checked="checked">

          </span>
          {!! Form::text('credito_dias', null, ['class' => 'form-control input-sm', 'data-inputmask'=>"'mask': '9','repeat': 4, 'greedy': false"]) !!}
            
            @else
            <span class="input-group-addon">

            <input name="forma_pago" type="checkbox" value="Credito" ng-model="forma_pago2">

          </span>
          {!! Form::text('credito_dias', null, ['class' => 'form-control input-sm', 'ng-disabled'=>"!forma_pago2", 'data-inputmask'=>"'mask': '9','repeat': 4, 'greedy': false"]) !!}

          @endif
      </div>


    </div>

    <div class="col-md-2">
      <label for="">Cheque</label>
      <div class="input-group">
      @if ($factura->forma_pago  =="Cheque")
        <span class="input-group-addon">
          <input name="forma_pago" type="checkbox" value="Cheque" checked="checked" >
        </span>
        {!! Form::text('cheque_numero', null, ['class' => 'form-control input-sm', 'data-inputmask'=>"'mask': '9', 'repeat': 11, 'greedy': false"]) !!}
      
        @else
        <span class="input-group-addon">
          <input name="forma_pago" type="checkbox" value="Cheque" ng-model="forma_pago3">
        </span>
        {!! Form::text('cheque_numero', null, ['class' => 'form-control input-sm', 'ng-disabled'=>"!forma_pago3", 'data-inputmask'=>"'mask': '9', 'repeat': 11, 'greedy': false"]) !!}
      

        @endif
      </div>
    </div>
    <div class="col-md-2">
      <label for="">Banco</label>
      <div class="input-group">
      @if ($factura->forma_pago  =="Cheque")
        <span class="input-group-addon">
          {{-- {!! Form::radio('forma_pago', "Banco", ['class' => 'form-control input-sm']) !!} --}}
          <input name="forma_pago" type="checkbox" value="Banco" checked="checked">

        </span>
        {!! Form::text('banco_cuenta', null, ['class' => 'form-control input-sm', 'data-inputmask'=>"'mask': '(9999)-9999-99-9999999999', 'greedy': false"]) !!}
      @else

       <span class="input-group-addon">
          {{-- {!! Form::radio('forma_pago', "Banco", ['class' => 'form-control input-sm']) !!} --}}
          <input name="forma_pago" type="checkbox" value="Banco" ng-model="forma_pago4">

        </span>
        {!! Form::text('banco_cuenta', null, ['class' => 'form-control input-sm', 'ng-disabled'=>"!forma_pago4", 'data-inputmask'=>"'mask': '(9999)-9999-99-9999999999', 'greedy': false"]) !!}

      @endif

      </div>
    </div>


  </div>

  <br>
  <div class="row no-print">
    <div class="col-xs-6">
      {{ Form::button('<i class="fa fa-save"><span> Guardar </span> </i>', ['type' => 'submit', 'class' => 'btn btn-success btn-lg', 'ng-disabled'=>'formulario.$invalid'] )  }}
      <a href="{{URL::to('home')}}" class="btn btn-danger  btn-lg"> <i class="fa fa-undo"></i> <span>Cancelar</span></a>

    </div>
  </div>
</section>
<!-- /.content -->
<div class="clearfix"></div>
</div>

<!-- /.content-wrapper -->