@extends('layouts.admin')

@section('content')
        

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($factura, ['route' => ['facturas.update', $factura->id], 'method' => 'patch']) !!}

            @include('facturas.fields_edit')

            {!! Form::close() !!}
        </div>
@endsection
