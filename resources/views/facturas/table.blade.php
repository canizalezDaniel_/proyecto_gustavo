<table class="table table-responsive" id="facturas-table">
    <thead>
        <th>Razon Social</th>
        <th>Direccion Fiscal</th>
        <th>Ci Rif</th>
        <th>Fecha Emision</th>
        <th>Telefono</th>
        <th>Nota Entrega Codigo</th>
        <th>Condicion Pago</th>
        <th>Dias</th>
        <th>Forma Pago</th>
        <th>Numero Cheque</th>
        <th>Banco</th>
        <th>Credito Dias</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($facturas as $factura)
        <tr>
            <td>{!! $factura->razon_social !!}</td>
            <td>{!! $factura->direccion_fiscal !!}</td>
            <td>{!! $factura->ci_rif !!}</td>
            <td>{!! $factura->fecha_emision !!}</td>
            <td>{!! $factura->telefono !!}</td>
            <td>{!! $factura->nota_entrega_codigo !!}</td>
            <td>{!! $factura->condicion_pago !!}</td>
            <td>{!! $factura->dias !!}</td>
            <td>{!! $factura->forma_pago !!}</td>
            <td>{!! $factura->numero_cheque !!}</td>
            <td>{!! $factura->banco !!}</td>
            <td>{!! $factura->credito_dias !!}</td>
            <td>
                {!! Form::open(['route' => ['facturas.destroy', $factura->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('facturas.show', [$factura->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('facturas.edit', [$factura->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Deseas borrar el registro seleccionado?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
