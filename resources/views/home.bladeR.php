@extends('layouts.admin')

 @section('content')

@include('flash::message')

 <div class="row">
                
            <!-- /.row -->
            <div class="col-lg-10  col-md-offset-1">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <center><img src="imagenes/notas.png" class="img-thumbnail" width="180" height="180"></center>
                            </div>
                        </div>
                        <a href="{{URL::to('notasEntregas/create')}}">
                            <div class="panel-footer">
                               <center><button class="btn btn-danger">NOTA DE ENTREGA</button></center>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <center><img src="imagenes/factura2.png" class="img-thumbnail" width="180" height="180"></center>
                            </div>
                        </div>
                        <a href="{{URL::to('facturas/create')}}">
                            <div class="panel-footer">
                               <center><button class="btn btn-danger">FACTURACIÓN</button></center>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <center><img src="imagenes/clientes.png" class="img-thumbnail" width="180" height="180"></center>
                            </div>
                        </div>
                        <a href="{{URL::to('clientes')}}">
                            <div class="panel-footer">
                               <center><button class="btn btn-danger">CLIENTES</button></center>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <center><img src="imagenes/impresora.png" class="img-thumbnail" width="180" height="180"></center>
                            </div>
                        </div>
                        <a href="{{URL::to('products')}}">
                            <div class="panel-footer">
                               <center><button class="btn btn-danger">PRODUCTOS</button></center>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

@endsection
