<table class="table table-responsive" id="notasDebitos-table">
    <thead>
        <th>Lugar Emision</th>
        <th>Empresa Nombre</th>
        <th>Empresa Direccion</th>
        <th>Fecha Emision</th>
        <th>Ci Rif</th>
        <th>Telefono</th>
        <th>Nota Debito Codigo</th>
        <th>Recibido Conforme</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($notasEntregas as $notasEntrega)
        <tr>
            <td>{!! $notasEntrega->lugar_emision !!}</td>
            <td>{!! $notasEntrega->empresa_nombre !!}</td>
            <td>{!! $notasEntrega->empresa_direccion !!}</td>
            <td>{!! $notasEntrega->fecha_emision !!}</td>
            <td>{!! $notasEntrega->ci_rif !!}</td>
            <td>{!! $notasEntrega->telefono !!}</td>
            <td>{!! $notasEntrega->nota_codigo !!}</td>
            <td>{!! $notasEntrega->recibido_conforme !!}</td>
            <td>
                {!! Form::open(['route' => ['notasEntregas.destroy', $notasEntrega->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('notasEntregas.show', [$notasEntrega->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('notasEntregas.edit', [$notasEntrega->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
