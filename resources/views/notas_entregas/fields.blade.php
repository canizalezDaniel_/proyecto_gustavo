<!-- Main content -->
<section class="invoice">
  <!-- title row -->

  <div class="row">
    
    <div class="col-xs-12">
    <a class="btn btn-default btn-sm" href="javascript:history.go(-1)"><label class="fa fa-undo"></label> Atras!</a>
      <h2>Crear nota de entrega
              <small>#00-00{{$codigo_nota}}</small>

      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    @include('layouts.cabecera')
    {!! Form::hidden('nota_codigo', '000'.$codigo_nota, ['class' => 'form-control', 'placeholder'=>'Nota Nº']) !!}
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row" >

    <div class="col-xs-12 table-responsive">
      <a href ng-click="addItem()" class="btn btn-danger pull-left"><i class="fa fa-plus-circle"> </i> <span> Agregar Item</span></a>

      <table class="table table-striped" >
        <thead>
          <tr>
            <th width="10%">ITEM</th>
            <th  width="20%">PRODUCTO</th>
            {{--<th width="20%">NOTA</th>--}}
            <th width="10%">CANTIDAD SOLICITADA</th>
            <th width="10%">ACCIONES</th>

          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="item in invoice.items">
            <td>
              {{-- <input type="number" name="cantidad" class="form-control" /> --}}
              {!! Form::number('item[]', null, ['class' => 'form-control', 'ng-model'=>'item.item', 'required']) !!}
            </td>
            <td>
                <div class="form-group">

                    <select class="form-control" name="productos_id[]" ng-model="item.productos">
                        <option value="">Seleccione un producto</option>
                        @foreach($productos as $producto)
                            <option value="{{$producto->id}}">{{$producto->codigo}} - {{$producto->descripcion}}</option>

                        @endforeach

                    </select>

                </div>
            </td>
            {{--<td>
              --}}{{-- <input type="text" name="descripcion" class="form-control" /> --}}{{--
              --}}{{-- <textarea name="name" rows="3" cols="4" class="form-control"></textarea> --}}{{--
              {!! Form::textarea('observaciones[]', null, ['class' => 'form-control', 'rows'=>'3', 'cols'=>'1']) !!}

            </td>--}}

            <td>
              {!! Form::number('cantidad[]', null, ['class' => 'form-control', 'ng-model'=>'item.cantidad', 'required']) !!}


            </td>

            <td ng-if="item.item >'1' ">
              <a href ng-click="removeItem($index)" class="btn btn-danger"><em class="fa fa-trash"></em></a>
            </td>

          </tr>
        </tbody>
      </table>
    </div>
    <!-- /.col -->


  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-xs-12">
      <h5 class="page-header"> Recibido por
      </h5>
    </div>
    <div class="col-xs-6">
      {{--  Info 1--}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Nombre y apellido:</label>
        {!! Form::text('recibido_por', null, ['class' => 'form-control', 'placeholder'=>'Nombre y apellido']) !!}

      </div>

    </div>

    <div class="col-xs-12">
      <h2 class="page-header">
        Datos de la empresa transportista
      </h2>
    </div>
    <!-- /.col -->
    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Nombre</label>
        {!! Form::text('nombre_transportista', null, ['class' => 'form-control', 'placeholder'=>'Nombre del transportista']) !!}

      </div>

    </div>


    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Cedula:</label>
        {!! Form::text('cedula_transportista', null, ['class' => 'form-control', 'placeholder'=>'Cedula de identidad']) !!}

      </div>




    </div>

    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Empresa:</label>
        {!! Form::text('empresa_transportista', null, ['class' => 'form-control', 'placeholder'=>'Nombre empresa']) !!}

      </div>
    </div>

    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Vehiculo:</label>
        {!! Form::text('vehiculo_empresa', null, ['class' => 'form-control', 'placeholder'=>'Vehiculo de la empresa']) !!}

      </div>
    </div>

    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Marca:</label>
        {!! Form::text('marca_vehiculo', null, ['class' => 'form-control', 'placeholder'=>'Marca del vehiculo']) !!}

      </div>
    </div>

    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Placa:</label>
        {!! Form::text('placa_vehiculo', null, ['class' => 'form-control', 'placeholder'=>'Placa del vehiculo']) !!}

      </div>
    </div>

    <div class="col-xs-12">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        {!! Form::textarea('observaciones_nota', null, ['class' => 'form-control', 'rows'=>'2', 'cols'=>'5', 'placeholder'=>'Observaciones']) !!}

      </div>
    </div>


    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-6">

      <!-- Submit Field -->
      {{ Form::button('<i class="fa fa-save"><span> Guardar </span> </i>', ['type' => 'submit', 'class' => 'btn btn-success btn-lg', 'ng-disabled'=>'formulario.$invalid'] )  }}

      <a href="{{URL::to('home')}}" class="btn btn-danger  btn-lg"> <i class="fa fa-undo"></i> <span>Cancelar</span></a>

    </div>
  </div>
</section>
<!-- /.content -->

<!-- /.content-wrapper -->
