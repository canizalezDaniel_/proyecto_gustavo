@extends('layouts.admin')

@section('content')


    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'notasEntregas.store', 'name'=>'formulario', 'novalidate']) !!}


            @include('notas_entregas.fields')


        {!! Form::close() !!}
    </div>
@endsection
