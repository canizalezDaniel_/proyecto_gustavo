<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2>Editar nota de entrega
              <small>#{{$codigo_nota}}</small>

      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    

<div class="col-sm-6 invoice-col">
  {{-- Formulario --}}

  <div class="form-group"  ng-class="{ 'has-error' : formulario.lugar_emision.$invalid && !formulario.lugar_emision.$pristine }">
    {{-- <label for="exampleInputEmail1">Lugar de Emisión</label> --}}
    <label for="">Lugar de emisión:</label>
    {!! Form::text('lugar_emision', null, ['class' => 'form-control input-sm']) !!}
    {{-- <p ng-show="formulario.lugar_emision.$invalid && !formulario.lugar_emision.$pristine" class="help-block">Lugar de emisión requerido.</p> --}}

  </div>

  <div class="form-group" ng-class="{ 'has-error' : formulario.empresa_nombre.$invalid && !formulario.empresa_nombre.$pristine }">
    <label for="">Empresa:</label>

  @foreach($cliente_nota_entrega as $cliente)
        {!! Form::text('cliente', $cliente->nombre_razon_social, ['class' => 'form-control input-sm', 'READONLY']) !!}
      {!! Form::hidden('empresa_nombre',  $cliente->id, ['class' => 'form-control input-sm', 'READONLY']) !!}
    
  @endforeach

        
  </div>


</div>

<!-- /.col -->
<div class="col-sm-3 invoice-col">
  {{-- Formulario 2 --}}

  <div class="form-group" ng-class="{ 'has-error' : formulario.fecha_emision.$invalid && !formulario.fecha_emision.$pristine }">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">Fecha emisión:</label>
    {!! Form::text('fecha_emision', Carbon\Carbon::today()->toDateString(), ['class' => 'form-control input-sm', 'placeholder'=>'Fecha', 'id'=> 'datepicker']) !!}
    {{-- <p ng-show="formulario.fecha_emision.$invalid && !formulario.fecha_emision.$pristine" class="help-block">Fecha requerida.</p> --}}

  </div>


</div>

<div class="col-sm-3 invoice-col">
  {{-- Formulario 3 --}}

  <div class="form-group">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">Telefono:</label>
    {!! Form::text('telefono', null, ['class' => 'form-control input-sm', 'placeholder'=>'Numero de telefono', 'data-inputmask'=>'"alias: dd/mm/yyyy"','data-mask', 'READONLY']) !!}



  </div>


</div>

<div class="col-sm-6 invoice-col">
  {{-- Formulario 4 --}}

  <div class="form-group">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">RIF/C.I.:</label>
    {!! Form::text('ci_rif', null, ['class' => 'form-control input-sm', 'placeholder'=>'RIF/CI', 'READONLY']) !!}

  </div>


</div>


<!-- /.col -->
<div class="col-sm-12 invoice-col">
  {{-- Formulario 3 --}}
  <div class="form-group">
    {{-- <label for="exampleInputPassword1">Password</label> --}}
    <label for="">Domicilió fiscal:</label>
    {!! Form::text('empresa_direccion', null, ['class' => 'form-control input-sm', 'placeholder'=>'Domicilió Fiscal:', 'READONLY']) !!}

  </div>
</div>
<!-- /.col -->

    {!! Form::hidden('nota_codigo', $codigo_nota, ['class' => 'form-control', 'placeholder'=>'Nota Nº']) !!}
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row" >

    <div class="col-xs-12 table-responsive">
      {{-- <a href ng-click="addItem()" ng-init="clic=0" ng-model="clic"  class="btn btn-danger pull-left"><i class="fa fa-plus-circle"> </i> <span> Agregar Item</span></a> --}}

      <table class="table table-striped" >
        <thead>
          <tr>
            <th width="10%">Item</th>
            
           <th width="30%">Producto</th>
            <th width="10%">Cantidad</th>
       

            <!-- <th width="10%">Acciones</th> -->

          </tr>
        </thead>
        <tbody>
          @foreach($items_notas_entregas as $items)
          <tr>
            <td>
              {{-- <input type="number" name="cantidad" class="form-control" /> --}}
               {!! Form::hidden('id[]',  $items->id, ['class' => 'form-control']) !!}
              {!! Form::number('item[]',  $items->item, ['class' => 'form-control']) !!}
            </td>
            
           <td>
             
            <select class="form-control" name="productos_id[]" >
                <option value="">Seleccione un producto</option>
                
     
                @foreach($productos as $producto)
                    <option value="{{$producto->id}}"
                    @if($producto->id==$items->productos_id)
                            selected='selected'>{{$producto->descripcion}}
                    </option>
                    @else
                        <option value="{{$producto->id}}">{{$producto->descripcion}}</option>
                    @endif
                @endforeach

            </select>
              <!-- {!! Form::text('producto[]', $items->producto, ['class' => 'form-control']) !!}
               -->

            </td>
            <td>
              {!! Form::number('cantidad[]', $items->cantidad, ['class' => 'form-control']) !!}


            </td>
            
            <!-- <td ng-if="item.item >'1'">
              <a href ng-click="removeItem($index)" class="btn btn-danger"><em class="fa fa-trash"></em></a>
            </td> -->

          </tr>
          @endforeach
          
         </tbody>
      </table>
    </div>
    <!-- /.col -->


  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-xs-12">
      <h5 class="page-header"> Recibido por
      </h5>
    </div>
    <div class="col-xs-6">
      {{--  Info 1--}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Nombre y apellido:</label>
        {!! Form::text('recibido_por', null, ['class' => 'form-control', 'placeholder'=>'Nombre y apellido']) !!}

      </div>

    </div>

    <div class="col-xs-12">
      <h2 class="page-header">
        Datos de la empresa transportista
      </h2>
    </div>
    <!-- /.col -->
    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Nombre</label>
        {!! Form::text('nombre_transportista', null, ['class' => 'form-control', 'placeholder'=>'Nombre del transportista']) !!}

      </div>

    </div>


    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Cedula:</label>
        {!! Form::text('cedula_transportista', null, ['class' => 'form-control', 'placeholder'=>'Cedula de identidad']) !!}

      </div>




    </div>

    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Empresa:</label>
        {!! Form::text('empresa_transportista', null, ['class' => 'form-control', 'placeholder'=>'Nombre empresa']) !!}

      </div>
    </div>

    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Vehiculo:</label>
        {!! Form::text('vehiculo_empresa', null, ['class' => 'form-control', 'placeholder'=>'Vehiculo de la empresa']) !!}

      </div>
    </div>

    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Marca:</label>
        {!! Form::text('marca_vehiculo', null, ['class' => 'form-control', 'placeholder'=>'Marca del vehiculo']) !!}

      </div>
    </div>

    <div class="col-xs-2">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        <label for="">Placa:</label>
        {!! Form::text('placa_vehiculo', null, ['class' => 'form-control', 'placeholder'=>'Placa del vehiculo']) !!}

      </div>
    </div>

    <div class="col-xs-12">
      {{-- Info 2 --}}
      <div class="form-group">
        {{-- <label for="exampleInputEmail1">Email address</label> --}}
        {!! Form::textarea('observaciones_nota', null, ['class' => 'form-control', 'rows'=>'2', 'cols'=>'5', 'placeholder'=>'Observaciones']) !!}

      </div>
    </div>


    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-6">

      <!-- Submit Field -->
      {{ Form::button('<i class="fa fa-save"><span> Guardar </span> </i>', ['type' => 'submit', 'class' => 'btn btn-success btn-lg', 'ng-disabled'=>'formulario.$invalid'] )  }}

      <a href="{{URL::to('subnotas')}}" class="btn btn-danger  btn-lg"> <i class="fa fa-undo"></i> <span>Cancelar</span></a>

    </div>
  </div>
</section>
<!-- /.content -->
<div class="clearfix"></div>
</div>
<!-- /.content-wrapper -->
