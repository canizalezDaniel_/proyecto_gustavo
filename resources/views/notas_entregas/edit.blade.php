@extends('layouts.admin')

@section('content')
        
        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($notasEntrega, ['route' => ['notasEntregas.update', $notasEntrega->id], 'method' => 'patch']) !!}

            {{-- @include('notas_entregas.fields') --}}
            {{-- Crear otro formulario para editar  --}}
            @include('notas_entregas.fields_edit') 
            {!! Form::close() !!}
        </div>
@endsection
