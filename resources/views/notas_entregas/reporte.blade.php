@extends('layouts.admin')

@section('content')

@include('flash::message')

<div class="box">
<section class="content-header">
      <h1>
        Reportes
        <small>Notas de entregas</small>
      </h1>
 </section>
            <div class="box-header">
            </div>
            <div class="box-body table-responsive no-padding">
              <table id="example1" class="table table-bordered table-hover dataTable" role="grid">
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">CODÍGO NOTA</th>
                    <th>
                      ESTATUS
                    </th>
                    <th>EMPRESA</th>
                    <th>RIF/CI</th>
                    <th>FECHA EMISIÓN</th>
                    <th>ACCIONES</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($notas_entrega as $nota)
                    <tr>
                      <td><strong>{{$nota->nota_codigo}}</strong></td>
                      <td>
                        @if($nota->estatus==1)
                          <span class="label label-success">ACTIVA</span>
                        @else
                          <span class="label label-warning"> ANULADA </span>
                        @endif
                      </td>
                      <td>{{$nota->nombre_razon_social}}</td>
                      <td>{{$nota->ci_rif}}</td>
                      <td>
                        <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $nota->fecha_emision)->formatLocalized('%d %b %Y'); ?>
                      </td>
                      <td>
                        {!! Form::open(['route' => ['notasEntregas.destroy', $nota->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="impresoraPdf/{{$nota->id}}" target="_blank" class='btn btn-default btn-sm'><i class="fa fa-print" aria-hidden="true"></i></a>
                          <a href="{!! route('notasEntregas.edit', [$nota->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                          <a href="anularNotae/{{$nota->id}}" class='btn btn-warning btn-sm' onclick="return confirm('¿Desea anular el registro seleccionado ?')"><i class="fa fa-ban" aria-hidden="true"></i></a>

                          {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Desea eliminar el registro seleccionado ?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="col-xs-12">
                Página {{$notas_entrega->currentPage()}}  de {{ $notas_entrega->lastPage()}}
                <ul class="pagination hidden-xs pull-right">
                  {{ $notas_entrega->links() }}
                </ul>
              </div>
            </div>
            <!-- /.box-body -->
          </div>

@endsection