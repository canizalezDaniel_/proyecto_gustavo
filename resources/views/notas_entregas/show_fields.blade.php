<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $notasEntrega->id !!}</p>
</div>

<!-- Lugar Emision Field -->
<div class="form-group">
    {!! Form::label('lugar_emision', 'Lugar Emision:') !!}
    <p>{!! $notasEntrega->lugar_emision !!}</p>
</div>

<!-- Empresa Nombre Field -->
<div class="form-group">
    {!! Form::label('empresa_nombre', 'Empresa Nombre:') !!}
    <p>{!! $notasEntrega->empresa_nombre !!}</p>
</div>

<!-- Empresa Direccion Field -->
<div class="form-group">
    {!! Form::label('empresa_direccion', 'Empresa Direccion:') !!}
    <p>{!! $notasEntrega->empresa_direccion !!}</p>
</div>

<!-- Fecha Emision Field -->
<div class="form-group">
    {!! Form::label('fecha_emision', 'Fecha Emision:') !!}
    <p>{!! $notasEntrega->fecha_emision !!}</p>
</div>

<!-- Ci Rif Field -->
<div class="form-group">
    {!! Form::label('ci_rif', 'Ci Rif:') !!}
    <p>{!! $notasEntrega->ci_rif !!}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Telefono:') !!}
    <p>{!! $notasEntrega->telefono !!}</p>
</div>

<!-- Nota Codigo Field -->
<div class="form-group">
    {!! Form::label('nota_codigo', 'Nota Codigo:') !!}
    <p>{!! $notasEntrega->nota_codigo !!}</p>
</div>

<!-- Recibido Por Field -->
<div class="form-group">
    {!! Form::label('recibido_por', 'Recibido Por:') !!}
    <p>{!! $notasEntrega->recibido_por !!}</p>
</div>

<!-- Nombre Transportista Field -->
<div class="form-group">
    {!! Form::label('nombre_transportista', 'Nombre Transportista:') !!}
    <p>{!! $notasEntrega->nombre_transportista !!}</p>
</div>

<!-- Empresa Transportista Field -->
<div class="form-group">
    {!! Form::label('empresa_transportista', 'Empresa Transportista:') !!}
    <p>{!! $notasEntrega->empresa_transportista !!}</p>
</div>

<!-- Vehiculo Empresa Field -->
<div class="form-group">
    {!! Form::label('vehiculo_empresa', 'Vehiculo Empresa:') !!}
    <p>{!! $notasEntrega->vehiculo_empresa !!}</p>
</div>

<!-- Placa Vehiculo Field -->
<div class="form-group">
    {!! Form::label('placa_vehiculo', 'Placa Vehiculo:') !!}
    <p>{!! $notasEntrega->placa_vehiculo !!}</p>
</div>

<!-- Marca Vehiculo Field -->
<div class="form-group">
    {!! Form::label('marca_vehiculo', 'Marca Vehiculo:') !!}
    <p>{!! $notasEntrega->marca_vehiculo !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $notasEntrega->observaciones !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $notasEntrega->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $notasEntrega->updated_at !!}</p>
</div>

