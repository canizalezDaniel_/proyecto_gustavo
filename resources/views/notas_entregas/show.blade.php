@extends('layouts.app')

@section('content')
    @include('notas_entregas.show_fields')

    <div class="form-group">
           <a href="{!! route('notasEntregas.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
