@extends('layouts.admin')

@section('content')

  @include('flash::message')


  <div class="col-md-12">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"> <span> <strong> NOTAS DE ENTREGA  </strong></span></a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> <span> <strong> NOTAS DE DÉBITO  </strong></span></a></li>
        <li><a href="#tab_3" data-toggle="tab"> <span> <strong> NOTAS DE CRÉDITO  </strong></span></a></li>
        <li><a href="#tab_4" data-toggle="tab"> <span> <strong> FACTURAS </strong></span></a></li>

        <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="box">
            <div class="box-header">



            </div>



            <div class="box-body table-responsive no-padding">
              <table id="example1" class="table table-bordered table-hover dataTable" role="grid">
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">CODÍGO NOTA</th>
                    <th>
                      ESTATUS
                    </th>
                    <th>EMPRESA</th>
                    <th>RIF/CI</th>
                    <th>FECHA EMISIÓN</th>
                    <th>ACCIONES</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($notas_entrega as $nota)
                    <tr>
                      <td><strong>{{$nota->nota_codigo}}</strong></td>
                      <td>
                        @if($nota->estatus==1)
                          <span class="label label-success">ACTIVA</span>
                        @else
                          <span class="label label-warning"> ANULADA </span>
                        @endif
                      </td>
                      <td>{{$nota->nombre_razon_social}}</td>
                      <td>{{$nota->ci_rif}}</td>
                      <td>
                        <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $nota->fecha_emision)->formatLocalized('%d %b %Y'); ?>
                      </td>
                      <td>
                        {!! Form::open(['route' => ['notasEntregas.destroy', $nota->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="impresoraPdf/{{$nota->id}}" target="_blank" class='btn btn-default btn-sm'><i class="fa fa-print" aria-hidden="true"></i></a>
                          <a href="{!! route('notasEntregas.edit', [$nota->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                          <a href="anularNotae/{{$nota->id}}" class='btn btn-warning btn-sm' onclick="return confirm('¿Desea anular el registro seleccionado ?')"><i class="fa fa-ban" aria-hidden="true"></i></a>

                          {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Desea eliminar el registro seleccionado ?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="col-xs-12">
                Página {{$notas_entrega->currentPage()}}  de {{ $notas_entrega->lastPage()}}
                <ul class="pagination hidden-xs pull-right">
                  {{ $notas_entrega->links() }}
                </ul>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
          <div class="box">
            <div class="box-header">




            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="example2" class="table table-bordered table-hover dataTable" role="grid">
                <thead>
                  <th>CODÍGO NOTA</th>
                  <th>
                    ESTATUS
                  </th>
                  <th>EMPRESA</th>
                  <th>RIF/CI</th>
                  <th>FECHA EMISIÓN</th>
                  <th>ACCIONES</th>
                </thead>
                <tbody>
                  @foreach($notas_debito as $nota_debito)
                    <tr>
                      <td><strong>{{$nota_debito->nota_debito_codigo}}</strong></td>
                      <td>
                        @if($nota_debito->estatus==1)
                          <span class="label label-success">ACTIVA</span>
                        @else
                          <span class="label label-warning"> ANULADA </span>
                        @endif

                      </td>
                      <td>{{$nota_debito->nombre_razon_social}}</td>
                      <td>{{$nota_debito->ci_rif}}</td>
                      <td>
                          <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $nota_debito->fecha_emision)->formatLocalized('%d %b %Y'); ?>
                      </td>
                      <td>
                        {!! Form::open(['route' => ['notasDebitos.destroy', $nota_debito->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="impresoraPdfnota/{{$nota_debito->id}}" target="_blank" class='btn btn-default btn-sm'><i class="fa fa-print" aria-hidden="true"></i></a>
                          <a href="anularNotad/{{$nota_debito->id}}" class='btn btn-warning btn-sm' onclick="return confirm('¿Desea anular el registro seleccionado ?')"><i class="fa fa-ban" aria-hidden="true"></i></a>
                          {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Desea eliminar el registro seleccionado ?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="col-xs-12">
                Página {{$notas_debito->currentPage()}}  de {{ $notas_debito->lastPage()}}
                <ul class="pagination hidden-xs pull-right">
                  {{ $notas_debito->links() }}
                </ul>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">
          <div class="box">
            <div class="box-header">


            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="example3" class="table table-bordered table-hover dataTable" role="grid">
                <thead>
                  <th>CODÍGO NOTA</th>
                  <th>
                    ESTATUS
                  </th>
                  <th>EMPRESA</th>
                  <th>RIF/CI</th>
                  <th>FECHA EMISIÓN</th>
                  <th>ACCIONES</th>
                </thead>
                <tbody>
                  @foreach($notas_credito as $nota_credito)
                    <tr>
                      <td><strong>{{$nota_credito->nota_credito_codigo}}</strong></td>
                      <td>
                        @if($nota_credito->estatus==1)
                          <span class="label label-success">ACTIVA</span>
                        @else
                          <span class="label label-warning"> ANULADA </span>
                        @endif
                      </td>
                      <td>{{$nota_credito->nombre_razon_social}}</td>
                      <td>{{$nota_credito->ci_rif}}</td>
                      <td>
                        <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $nota_credito->fecha_emision)->formatLocalized('%d %b %Y'); ?>
                      </td>
                      <td>
                        {!! Form::open(['route' => ['notasCreditos.destroy', $nota_credito->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="impresoraPdfnotac/{{$nota_credito->id}}" target="_blank" class='btn btn-default btn-sm'><i class="fa fa-print" aria-hidden="true"></i></a>
                          <a href="anularNotac/{{$nota_credito->id}}" class='btn btn-warning btn-sm' onclick="return confirm('¿Desea anular el registro seleccionado ?')"><i class="fa fa-ban" aria-hidden="true" ></i></a>

                          {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Desea eliminar el registro seleccionado ?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="col-xs-12">
                Página {{$notas_credito->currentPage()}}  de {{ $notas_credito->lastPage()}}
                <ul class="pagination hidden-xs pull-right">
                  {{ $notas_credito->links() }}
                </ul>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.tab-pane -->


        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_4">
          <div class="box">
            <div class="box-header">


            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="example4" class="table table-bordered table-hover dataTable" role="grid">
                <thead>
                  <th>CODÍGO FACTURA</th>
                  <th>
                    ESTATUS
                  </th>
                  <th>EMPRESA</th>
                  <th>RIF/CI</th>
                  <th>
                    NOTA DE ENTREGA
                  </th>
                  <th>FECHA EMISIÓN</th>
                  <th>ACCIONES</th>
                </thead>
                <tbody>
                  @foreach($facturas as $factura)
                    <tr>
                      <td><strong>{{$factura->factura_codigo}}</strong></td>
                      <td>
                        @if($factura->estatus==1)
                          <span class="label label-success">ACTIVA</span>
                        @else
                          <span class="label label-warning"> ANULADA </span>
                        @endif
                      </td>
                      <td>{{$factura->nombre_razon_social}}</td>
                      <td>{{$factura->ci_rif}}</td>
                      <td>
                        @if($factura->nota_codigo=="")
                            <span class="label label-warning">Sin nota de entrega</span>
                        @else
                            <span class="label label-danger">{{$factura->nota_codigo}}</span>
                        @endif

                      </td>
                      <td>
                          <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $factura->fecha_emision)->formatLocalized('%d %b %Y'); ?>
                      </td>
                      <td>
                        {!! Form::open(['route' => ['facturas.destroy', $factura->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                          <a href="impresoraPdffactura/{{$factura->id}}" target="_blank" class='btn btn-default btn-sm'><i class="fa fa-print" aria-hidden="true"></i></a>
                          <a href="{!! route('facturas.edit', [$factura->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                          <a href="anularFactura/{{$factura->id}}" class='btn btn-warning btn-sm' onclick="return confirm('¿Desea anular el registro seleccionado ?')"><i class="fa fa-ban" aria-hidden="true"></i></a>

                          {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('¿Desea eliminar el registro seleccionado ?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="col-xs-12">
                {{-- Página {{$facturas->currentPage()}}  de {{ $facturas->lastPage()}}
                 --}}<ul class="pagination hidden-xs pull-right">
                  {{ $facturas->links()
                       
                    }}

                </ul>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
  </div>
@endsection
