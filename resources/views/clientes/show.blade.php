@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Información de clientes
        </h1>
    </section>
    <br/>
    @include('clientes.show_fields')
    <div class="form-group">
        <a class="btn btn-default btn-sm" href="javascript:history.go(-1)"><label class="fa fa-undo"></label> Atras!</a>

    </div>
@endsection
