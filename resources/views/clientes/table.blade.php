<div class="row">

    <section class="content-header">
        <h1>
            Administración de Clientes
            <small>Todos los clientes y proveedores de Molinos del Zulia</small>
        </h1>

    </section>
    <br/><br/>

<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Clientes y proveedores</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                </div>

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover" id="clientes-table">

                <thead>
                  <th>Razón Social</th>
                  <th>Dirección Fiscal</th>
                  <th>RIF</th>
                  <th>Teléfono</th>
                  <th colspan="3">Acciones</th>
                </thead>

              <tbody>
                @foreach($clientes as $clientes)
                    <tr>
                        <td>{!! $clientes->nombre_razon_social !!}</td>
                        <td>{!! str_limit($clientes->direccion_fiscal, $limit = 100, $end = '...') !!}</td>
                        <td>{!! $clientes->ci_rif !!}</td>
                        <td>{!! $clientes->telefono !!}</td>
                        <td>
                            {!! Form::open(['route' => ['clientes.destroy', $clientes->id], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                <a href="{!! route('clientes.show', [$clientes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="{!! route('clientes.edit', [$clientes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
              </tbody>
           </table>
            </div>

            <!-- /.box-body -->
          </div>
          <a class="btn btn-success pull-left" href="{!! route('clientes.create') !!}">Agregar cliente</a>

          <!-- /.box -->
        </div>
</div>
