
<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Cliente</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre o razón social</label>
                {!! Form::text('nombre_razon_social', null, ['class' => 'form-control']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputFile">RIF</label>
                {!! Form::text('ci_rif', null, ['class' => 'form-control', 'data-inputmask'=>"'mask': 'a-99999999-9', 'greedy': false"]) !!}


                <p class="help-block">RIF: J-99999999-9 ó CI:V-99999999-0</p>
            </div>


            <div class="form-group">
                <label for="">Estado:</label>
                <select class="form-control input-sm" name="estado">
                    <option value="">Seleccione un estado</option>
                    <option value="ZU">Zulia</option>
                    <option value="VA">Valencia</option>
                    <option value="CA">Caracas</option>
                </select>

            </div>

            <div class="form-group">
                <label for="">Ciudad:</label>
                <select class="form-control input-sm" name="ciudad">
                    <option value="">Seleccione una ciudad</option>
                    <option value="MA">Maracaibo</option>
                    <option value="CA">Carabobo</option>
                    <option value="CA">Caracas</option>
                </select>

            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Teléfono contacto</label>
                {!! Form::text('telefono', null, ['class' => 'form-control', 'data-inputmask'=>"'mask': '+58(9999)-9999999', 'greedy': false"]) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Dirección Fiscal</label>
                {!! Form::textarea('direccion_fiscal', null, ['class' => 'form-control', 'cols'=>"5", 'rows'=>'2']) !!}


            </div>

            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('clientes.index') !!}" class="btn btn-default">Cancelar</a>
        </div>
    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>
