
<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Cliente</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre o razón social</label>
                {!! Form::text('nombre_razon_social', $clientes->nombre_razon_social, ['class' => 'form-control', 'readonly']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputFile">RIF</label>
                {!! Form::text('ci_rif', $clientes->ci_rif, ['class' => 'form-control', 'data-inputmask'=>"'mask': 'a-99999999-9', 'greedy': false", 'readonly']) !!}


                <p class="help-block">RIF: J-99999999-9 ó CI:V-99999999-0</p>
            </div>


            <div class="form-group">
                <label for="">Estado:</label>
                <select class="form-control input-sm" name="estado">
                    <option value="">Seleccione un estado</option>
                    <option value="ZU">Zulia</option>
                    <option value="VA">Valencia</option>
                    <option value="CA">Caracas</option>
                </select>

            </div>

            <div class="form-group">
                <label for="">Ciudad:</label>
                <select class="form-control input-sm" name="ciudad">
                    <option value="">Seleccione una ciudad</option>
                    <option value="MA">Maracaibo</option>
                    <option value="CA">Carabobo</option>
                    <option value="CA">Caracas</option>
                </select>

            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Teléfono contacto</label>
                {!! Form::text('telefono', $clientes->telefono, ['class' => 'form-control', 'data-inputmask'=>"'mask': '+58(9999)-9999999', 'greedy': false", 'readonly']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Dirección Fiscal</label>
                {!! Form::textarea('direccion_fiscal', $clientes->direccion_fiscal, ['class' => 'form-control', 'cols'=>"5", 'rows'=>'2', 'readonly']) !!}


            </div>



        </div>
    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>

