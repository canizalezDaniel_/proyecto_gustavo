@extends('layouts.admin')

@section('content')

    @include('flash::message')

    <div class="row">

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{URL::to('notasEntregas/create')}}">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-folder"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Nota de entrega</span>
                        <span class="info-box-number">Crear nota de entrega</span>
                    </div>

                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{URL::to('facturas/create')}}">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-files-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Factura</span>
                        <span class="info-box-number">Crear factura</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ URL::to('inventarios') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-line-chart"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Inventario</span>
                        <span class="info-box-number"><small>Administrar inventario</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ URL::to('products') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-bar-chart"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Productos</span>
                        <span class="info-box-number"><small>Administrar productos</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Facturas mensuales por vendedor</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Vendedor</th>
                                <th>Cedula</th>
                                <th>Cantidad ventas</th>
                                <th>Año facturado</th>
                                <th>Mes facturado</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vendedores as $vendedor)
                            <tr>
                                <td><a href="#">{{$vendedor->nombre}}</a></td>
                                <td>{{$vendedor->ci}}</td>
                                <td><span class="label label-success">{{$vendedor->cantidad}}</span></td>
                                <td>{{$vendedor->anno}}</td>
                                <td>{{$vendedor->mes}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->

                <!-- /.box-footer -->
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Productos mayor facturado</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="morris-area-chart">
                                <div id="chart" style="height: 250px";>

                                </div>

                            </div>

                            <!-- ./chart-responsive -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <h4>Productos:</h4>
                            <ul class="chart-legend clearfix">
                                @if(isset($facturados))
                                @foreach($facturados as $facturado)
                                    <li><i class="fa fa-circle-o"></i> {{$facturado->descripcion}}</li>
                                @endforeach
                                @endif
                                {{--<li><i class="fa fa-circle-o text-red"></i> Chrome</li>
                                <li><i class="fa fa-circle-o text-green"></i> IE</li>
                                <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
                                <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                                <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
                                <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
--}}
                            </ul>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                {{--<div class="box-footer no-padding">
                    <ul class="nav nav-pills nav-stacked">

                        <li><a href="#">Factura Código 5555
                                <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 5 Productos</span></a></li>
                        <li><a href="#">Factura Código 6666 <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 10 Productos</span></a>
                        </li>

                    </ul>
                </div>--}}
                <!-- /.footer -->
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Últimos productos agregados</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @if(isset($productos))
                        @foreach($productos as $producto)
                        <li class="item">
                            <div class="product-img">
                                <img src="dist/img/default-50x50.gif" alt="Imagen del producto">
                            </div>
                            <div class="product-info">
                                <a href="javascript:void(0)" class="product-title">{{$producto->codigo}}
                                     <span class="label label-warning pull-right">{{$producto->cantidad}}</span></a>
                        <span class="product-description">
                          {{$producto->descripcion}}.
                        </span>
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="javascript:void(0)" class="uppercase">Ver todos los productos</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>




    </div>


@endsection
