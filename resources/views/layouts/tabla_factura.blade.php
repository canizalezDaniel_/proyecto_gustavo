<table class="table table-striped" width="100%">
    <thead>
    <tr>

        <th class="col-sm-2">PRODUCTO</th>
        <th class="col-sm-0">CANTIDAD DISP.</th>
        <th class="col-sm-0">CANTIDAD</th>
        <th class="col-sm-0">PRECIO UNITARIO(Bs)</th>
        <th class="col-sm-0">TOTAL Bs</th>
        <th>ACCIONES</th>

    </tr>
    </thead>
    <tbody>

    <tr ng-repeat="item in invoice.items">


        <td>
            <div class="form-group">

                <select class="form-control input-sm" name="productos_id[]" ng-model="item.productos" ng-model-options="{ debounce: 1000 }" ng-change="cambiarSeleccion()">
                    <option value="">Seleccione un producto</option>
                    @foreach($productos as $producto)

                        <option value="{{$producto->id}}">{{$producto->codigo}} - {{$producto->descripcion}}</option>

                    @endforeach

                </select>

            </div>

        </td>


        <td>
            {!! Form::text('cantidad_producto[]', null, ['class' => 'form-control input-sm', 'ng-model'=>'item.cantidad_producto', 'readonly', 'ng-value'=>'item.cantidad_producto']) !!}

        </td>

        <td>
            {!! Form::number('cantidad[]', null, ['class' => 'form-control input-sm', 'ng-model'=>'item.cantidad']) !!}

            <div ng-if="item.cantidad > item.cantidad_producto">
                <p ng-show=class="help-block">Cantidad no disponible.</p>
            </div>
        </td>

        <td>



            {!! Form::text('precio_unitario_bs[]', null, ['class' => 'form-control input-sm', 'readonly' ,'ng-model'=>'item.precio_unitario_bs', 'decimal-min'=>'0', 'input-decimal-separator'=>'3']) !!}




        </td>
        <td>



            {{--{!! Form::text('total_item_bs[]',@{{total_bolivares() | number:2 }}, ['class' => 'form-control input-sm','step'=>'any', 'ng-model'=>'item.total_item_bs']) !!}--}}
            {!! Form::text('total_item_bs[]', null, ['class' => 'form-control input-sm', 'readonly', 'ng-value'=>'item.precio_unitario_bs*item.cantidad | number:2']) !!}






        </td>
        <td ng-if="item.item >'1' ">
            <a href ng-click="removeItem($index)"><em class="fa fa-trash fa-2x"></em></a>
        </td>

    </tr>
    {{-- <tr>
     <td> </td>
      <td> </td>
      <td> </td>
      <td  class="text-right"> SUB-TOTAL</td>
      <td>
      {!! Form::text('sub_total_usd', '@{{total_usd() | number:2}}', ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td> </td>

    </tr>
    <tr>
    <td> </td>
      <td> </td>
      <td> </td>
      <td  class="text-right"> IVA USD {{number_format($iva*100)}} %</td>
      <td>  <input type="hidden" ng-model="iva">
      {!! Form::text('iva_usd', '@{{total_usd()*iva | number:2}}', ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td> </td>

    </tr>

    <tr>
     <td> </td>
      <td> </td>
        <td> </td>
      <td  class="text-right"> TOTAL USD</td>
      <td> {!! Form::text('total_usd', '@{{(total_usd()*iva)+total_usd() | number:2}}', ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td> </td>

    </tr> --}}

    {{-- <tr >
      <td colspan="5">
          <strong>NOTA: MONTO CALCULADO A UNA TASA DE CAMBIO DE {{$tasa_cambio}} Bs./$</strong>
         <textarea rows="8" cols="120" name="observaciones" id="T2" style="font-size: 10px">
           FAVOR REALIZAR TRANSFERENCIA A: SUSERCA<br>
           RIF: J-30387022-8 <br>
           BANCO OCCIDENTAR DE DESCUENTO <br>
           CUENTA CORRIENTE Nº 0116-0101-48-0015378756
         </textarea>
      </td>

       <td></td>
       <td></td>

    </tr> --}}
    <tr >
        <td colspan="4" class="text-right"></td>
        <td class="text-right">Total Base imponible Bs</td>
        <td>
            {!! Form::text('total_base', '@{{total_bs() | number:2 }}', ['class' => 'form-control input-sm', 'readonly']) !!}
        </td>
    </tr>
    <tr>
        <td colspan="4"></td>

        <td  class="text-right">Ajustes</td>
        <td> {!! Form::number('ajustes', null, ['class' => 'form-control input-sm', 'ng-model'=>'total_ajuste']) !!}
        </td>
        <td></td>

    </tr>

    {{--  <tr>
       <td colspan="4"></td>
       <td  class="text-right">Total Base imponible Bs</td>
       <td>
       {!! Form::text('total_base_ajustes', '@{{total_bs()+total_ajuste | number:2 }}', ['class' => 'form-control input-sm', 'readonly']) !!}
       </td>
      </tr> --}}
    <tr>
        <td colspan="4"></td>
        <td  class="text-right">IVA {{number_format($iva_bs_porcentaje*100)}}% Bs</td>
        <td>
            {!! Form::text('iva_bs', '@{{(total_bs()*iva_bs_porcentaje) | number:2 }}', ['class' => 'form-control input-sm', 'readonly']) !!}
        </td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td  class="text-right">Total a pagar</td>
        <td>
            {!! Form::text('total_bs', '@{{((total_bs()*iva_bs_porcentaje)+total_bs())+total_ajuste | number:2 }}', ['class' => 'form-control input-sm', 'readonly']) !!}
        </td>
    </tr>
    </tbody>
</table>
