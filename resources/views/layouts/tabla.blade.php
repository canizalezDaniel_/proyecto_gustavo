<table class="table table-striped">
  <thead>
    <tr>
      <th width="10%">CANTIDAD</th>
      <th  width="60%">DESCRIPCIÓN</th>
{{--       <th width="10%">Total $</th>
 --}}      <th  width="15%"> TOTAL</th>
      <th width="5%"></th>

    </tr>
  </thead>
  <tbody>
    <tr ng-repeat="item in invoice.items">
      <td>
        {{-- <input type="text" ng-model="item.cantidad" class="form-control" /> --}}
        <div class="input-group2" style="margin-top:45px">
        {!! Form::number('cantidad[]', null, ['class' => 'form-control', 'ng-model'=>'item.cantidad']) !!}
        </div>
      </td>
      <td>

       {!! Form::textarea('descripcion[]', null, ['class' => 'form-control', 'rows'=>'12', 'cols'=>'9', 'id'=>'T1', 'ng-model'=>'item.descripcion']) !!}

         <strong>NOTA: MONTO CALCULADO A UNA TASA DE CAMBIO DE {{$tasa_cambio}} Bs./$</strong>
      </td>

      <td>
        <div class="input-group" style="margin-top:45px">
        {{-- <input type="text" ng-model="item.bolivar" class="form-control" > --}}
        {!! Form::text('total_item_bs[]', null, ['class' => 'form-control', 'ng-model'=>'item.total_item_bs', 'decimal-min'=>'1', 'input-decimal-separator'=>'3']) !!}

      </div>
      </td>
      <td ng-if="item.item >'1' ">
        <a href ng-click="removeItem($index)" class="btn btn-danger"><em class="fa fa-trash"></em></a>
      </td>

    </tr>
    <tr>

      <td></td>
      <td  class="text-right">Total Base imponible Bs</td>
      <td> {!! Form::text('total_base', '@{{total_bolivares() | number:2 }}', ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td></td>

    </tr>
    <tr>

      <td></td>
      <td  class="text-right">Ajustes</td>
      <td> {!! Form::number('total_ajuste', null, ['class' => 'form-control input-sm', 'ng-model'=>'total_ajuste']) !!}
      </td>
      <td></td>

    </tr>

    <tr>

      <td></td>
      <td  class="text-right">Total Base imponible Bs</td>


      <td> {!! Form::text('total_base', '@{{total_bolivares()+total_ajuste | number:2 }}', ['class' => 'form-control input-sm', 'readonly']) !!}
      </td>
      <td></td>

    </tr>
    <tr>

      <td></td>
      <td  class="text-right">IVA {{number_format($iva_bs_porcentaje, 2)*100}}% Bs</td>
      <td> {!! Form::text('iva_bs', '@{{total_bolivares()*iva_porcetaje_bs | number:2 }}', ['class' => 'form-control input-sm', 'readonly']) !!}</td>
      <td></td>

    </tr>
    <tr>

      <td></td>
      <td  class="text-right">Total a pagar</td>
      <td> {!! Form::text('total_bs', '@{{(total_bolivares()*iva_porcetaje_bs)+total_bolivares()+total_ajuste | number:2 }}', ['class' => 'form-control input-sm', 'readonly']) !!}</td>
      <td></td>

    </tr>
  </tbody>
</table>
