<div class="col-sm-6 invoice-col">
  {{-- Formulario --}}

  <div class="form-group"  ng-class="{ 'has-error' : formulario.lugar_emision.$invalid && !formulario.lugar_emision.$pristine }">
    {{-- <label for="exampleInputEmail1">Lugar de Emisión</label> --}}
    <label for="">Lugar de emisión:</label>
    {!! Form::text('lugar_emision', null, ['class' => 'form-control input-sm', 'ng-model'=>'lugar_emision', 'required']) !!}
    {{-- <p ng-show="formulario.lugar_emision.$invalid && !formulario.lugar_emision.$pristine" class="help-block">Lugar de emisión requerido.</p> --}}

  </div>
  <div class="form-group" ng-class="{ 'has-error' : formulario.empresa_nombre.$invalid && !formulario.empresa_nombre.$pristine }">
    <label for="">Cliente:</label>
    <select class="form-control input-sm" name="clientes_id" id="empresa" ng-model="empresa_nombre" required>
      <option value="">Seleccione un cliente</option>
      @foreach($clientes as $cliente)
            <option value="{{$cliente->id}}">{{$cliente->nombre_razon_social}}</option>
        @endforeach
    </select>
    {{-- <p ng-show="formulario.empresa_nombre.$invalid && !formulario.empresa_nombre.$pristine" class="help-block">Empresa requerido.</p> --}}

  </div>

  <div class="form-group" ng-class="{ 'has-error' : formulario.empresa_nombre.$invalid && !formulario.empresa_nombre.$pristine }">
    <label for="">Vendedor:</label>
    <select class="form-control input-sm" name="vendedors_id" ng-model="vendedor" required>
      <option value="">Seleccione un vendedor</option>
      @foreach($vendedores as $vendedor)
      <option value="{{$vendedor->id}}">{{$vendedor->nombre}}</option>
      @endforeach
    </select>
    {{-- <p ng-show="formulario.empresa_nombre.$invalid && !formulario.empresa_nombre.$pristine" class="help-block">Empresa requerido.</p> --}}

  </div>


</div>

<!-- /.col -->
<div class="col-sm-3 invoice-col">
  {{-- Formulario 2 --}}

  <div class="form-group" ng-class="{ 'has-error' : formulario.fecha_emision.$invalid && !formulario.fecha_emision.$pristine }">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">Fecha emisión:</label>
    {!! Form::text('fecha_emision', Carbon\Carbon::today()->toDateString(), ['class' => 'form-control input-sm', 'placeholder'=>'Fecha', 'id'=> 'datepicker', 'ng-model'=>'fecha_emision', 'required']) !!}
    {{-- <p ng-show="formulario.fecha_emision.$invalid && !formulario.fecha_emision.$pristine" class="help-block">Fecha requerida.</p> --}}

  </div>


</div>

<div class="col-sm-3 invoice-col">
  {{-- Formulario 3 --}}

  <div class="form-group">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">Telefono:</label>
    {!! Form::text('telefono', null, ['class' => 'form-control input-sm', 'placeholder'=>'Numero de telefono', 'READONLY']) !!}



  </div>


</div>

<div class="col-sm-6 invoice-col">
  {{-- Formulario 4 --}}

  <div class="form-group">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">RIF/C.I.:</label>
    {!! Form::text('ci_rif', null, ['class' => 'form-control input-sm', 'placeholder'=>'RIF/CI', 'READONLY']) !!}

  </div>


</div>


<!-- /.col -->
<div class="col-sm-12 invoice-col">
  {{-- Formulario 3 --}}
  <div class="form-group">
    {{-- <label for="exampleInputPassword1">Password</label> --}}
    <label for="">Domicilió fiscal:</label>
    {!! Form::textarea('empresa_direccion', null, ['class' => 'form-control input-sm', 'placeholder'=>'Domicilió Fiscal:', 'rows'=>'2', 'cols'=>'4','READONLY']) !!}

  </div>
</div>
<!-- /.col -->

<!-- /.col -->
<div class="col-sm-6">
  {{-- Formulario 3 --}}

  <div class="form-group" ng-class="{ 'has-error' : formulario.condicion_pago.$invalid && !formulario.condicion_pago.$pristine }">
    {{-- <label for="exampleInputPassword1">Password</label> --}}

    <div class="col-md-2">
      <label for="">CONTADO</label>
      <div class="input-group input-group-sm">
          {!! Form::checkbox('condicion_pago', "Contado", true,['ng-model'=>'condicion_pago']) !!}
      </div>
    </div>
    <div class="col-md-4">
      <label for="">CRÉDITO</label>
      <div class="input-group nput-group-sm">
        <span class="input-group-addon">
          {{-- {!! Form::checkbox('condicion_pago', "Credito", ['class' => 'form-control input-sm', 'ng-model'=>'condicion_pago2', 'required']) !!} --}}
          <input name="condicion_pago" type="checkbox" value="Credito" ng-model="condicion_pago2">

        </span>
        {!! Form::text('credito_condicion_dias', null, ['class' => 'form-control input-sm', 'placeholder'=> 'Días', 'ng-disabled'=>"!condicion_pago2", 'data-inputmask'=>"'mask': '9', 'repeat': 11, 'greedy': false"]) !!}
      </div>
    </div>
    <div class="col-md-3" ng-class="{ 'has-error' : formulario.vencimiento.$invalid && !formulario.vencimiento.$pristine }">
      <label for="">VENCIMIENTO</label>
      <div class="input-group input-group-sm" >

        {!! Form::text('vencimiento', null, ['class' => 'form-control input-sm', 'id'=> 'datepicker2', 'ng-model'=>'vencimiento']) !!}

      </div>
    </div>

    {{-- <p ng-show="formulario.condicion_pago.$invalid && !formulario.condicion_pago.$pristine" class="help-block">Condición de pago requerida.</p> --}}

  </div>
</div>
<!-- /.col -->
