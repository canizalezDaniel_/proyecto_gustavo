

<div class="col-sm-6 invoice-col">
  {{-- Formulario --}}

  <div class="form-group"  ng-class="{ 'has-error' : formulario.lugar_emision.$invalid && !formulario.lugar_emision.$pristine }">
    {{-- <label for="exampleInputEmail1">Lugar de Emisión</label> --}}
    <label for="">Lugar de emisión:</label>
    {!! Form::text('lugar_emision', null, ['class' => 'form-control input-sm', 'ng-model'=>'lugar_emision', 'required']) !!}
    {{-- <p ng-show="formulario.lugar_emision.$invalid && !formulario.lugar_emision.$pristine" class="help-block">Lugar de emisión requerido.</p> --}}

  </div>

  <div class="form-group" ng-class="{ 'has-error' : formulario.empresa_nombre.$invalid && !formulario.empresa_nombre.$pristine }">
    <label for="">Empresa:</label>
    <select class="form-control input-sm" name="empresa_nombre" id="empresa" ng-model="empresa_nombre" required>
        <option value="">Seleccione un Cliente - Proveedor</option>
      @foreach($clientes as $cliente)
        <option value="{{$cliente->id}}">{{$cliente->nombre_razon_social}}</option>
      @endforeach
    </select>
    {{-- <p ng-show="formulario.empresa_nombre.$invalid && !formulario.empresa_nombre.$pristine" class="help-block">Empresa requerido.</p> --}}

  </div>


</div>

<!-- /.col -->
<div class="col-sm-3 invoice-col">
  {{-- Formulario 2 --}}

  <div class="form-group" ng-class="{ 'has-error' : formulario.fecha_emision.$invalid && !formulario.fecha_emision.$pristine }">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">Fecha emisión:</label>
    {!! Form::text('fecha_emision', Carbon\Carbon::today()->toDateString(), ['class' => 'form-control input-sm', 'placeholder'=>'Clic para ingresar fecha', 'id'=> 'datepicker', 'ng-model'=>'fecha_emision', 'required']) !!}
    {{-- <p ng-show="formulario.fecha_emision.$invalid && !formulario.fecha_emision.$pristine" class="help-block">Fecha requerida.</p> --}}

  </div>


</div>

<div class="col-sm-3 invoice-col">
  {{-- Formulario 3 --}}

  <div class="form-group">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">Telefono:</label>
    {!! Form::text('telefono', null, ['class' => 'form-control input-sm', 'data-inputmask'=>'"alias: dd/mm/yyyy"','data-mask', 'READONLY']) !!}



  </div>


</div>

<div class="col-sm-6 invoice-col">
  {{-- Formulario 4 --}}

  <div class="form-group">
    {{-- <label for="exampleInputEmail1">Email address</label> --}}
    <label for="">RIF/C.I.:</label>
    {!! Form::text('ci_rif', null, ['class' => 'form-control input-sm', 'READONLY']) !!}

  </div>


</div>


<!-- /.col -->
<div class="col-sm-12 invoice-col">
  {{-- Formulario 3 --}}
  <div class="form-group">
    {{-- <label for="exampleInputPassword1">Password</label> --}}
    <label for="">Domicilió Fiscal:</label>
    {!! Form::textarea('empresa_direccion', null, ['class' => 'form-control input-sm', 'rows'=>'2', 'cols'=>'4','READONLY']) !!}

  </div>
</div>
<!-- /.col -->
