<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Molinos el Zuliano</title>
  <!-- Responsivo con los dispositivos moviles -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  {!! Html::style('/bootstrap/css/bootstrap.min.css') !!}
  <!-- Font Awesome CDN-->
  {!! Html::style('dist/css/font-awesome.css') !!}

    <!-- Ionicons CDN
   {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') !!}-->
  <!-- Tema estilo principal -->
  {!! Html::style('dist/css/AdminLTE.min.css') !!}
  <!-- Selección del skin: red -->
  {!! Html::style('dist/css/skins/_all-skins.min.css') !!}
  <!-- Morris chart -->
{{--
  {!! Html::style('plugins/morris/morris.css') !!}
--}}

    <!-- jvectormap -->
  {{--   <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
 --}}  <!-- Date Picker -->
  {!! Html::style('plugins/datepicker/datepicker3.css') !!}
  {!! Html::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}


  <!-- Daterange picker
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">-->
  <!-- bootstrap wysihtml5 - text editor
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">-->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  [endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-app="miApp" ng-controller="CartForm">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="{{ URL::to('home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Molinos</b> el Zuliano</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <img src="{{asset("dist/img/user2-160x160.jpg")}}" class="user-image" alt="User Image">
               <span class="hidden-xs"> {{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{asset("dist/img/user2-160x160.jpg")}}" class="user-image" alt="User Image">
                <p>
                  {{ Auth::user()->name }}
                  <small>Administrador</small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-right">
                  <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset("imagenes/logo3.png")}}" class="img-circle" alt="Imagen" width="180" height="180">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> En linea</a>
            </div>
        </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="active treeview">
          <a href="{{ URL::to('home') }}">
            <i class="fa fa-dashboard"></i> <span>Principal</span>
          </a>
        </li>
          <li class="treeview">
              <a href="#">
                  <i class="fa fa-thumb-tack"></i> <span>Crear</span>
                <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu menu-open" style="display: block;">
                  <li><a href="{{ URL::to('notasEntregas/create') }}">
                          <i class="fa fa-folder"></i> <span>Nota de entrega</span>
                      </a>
                  </li>
                  <li> <a href="{{ URL::to('facturas/create') }}">
                          <i class="fa fa-files-o"></i> <span>Factura</span>
                      </a>
                  </li>
              </ul>
          </li>
        {{--<li class="treeview">
          <a href="{{ URL::to('notasEntregas/create') }}">
            <i class="fa fa-folder"></i> <span>Nota de entrega</span>
          </a>
        </li>
        <li class="treeview">
          <a href="{{ URL::to('facturas/create') }}">
            <i class="fa fa-files-o"></i> <span>Factura</span>
          </a>
        </li>--}}


        <li class="treeview">
          <a href="#">
            <i class="fa fa-print"></i> <span>Reportes</span>
                <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu menu-open" style="display: block;">
            <li><a href="{{URL::to('reporteNotae')}}"><i class="fa fa-file-pdf-o"></i> Notas de entrega</a></li>
            <li><a href="{{URL::to('reporteFactura')}}"><i class="fa fa-file-pdf-o"></i> Facturas</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="{{ URL::to('clientes') }}">
            <i class="fa fa-male"></i> <span>Clientes</span>
          </a>
        </li>
        <li class="treeview">
          <a href="{{ URL::to('vendedors') }}">
            <i class="fa fa-users"></i> <span>Vendedores</span>
          </a>
        </li>
        <li class="treeview">
          <a href="{{ URL::to('products') }}">
            <i class="fa fa-bar-chart"></i> <span>Productos</span>
          </a>
        </li>
        <li class="treeview">
              <a href="{{ URL::to('inventarios') }}">
                  <i class="fa fa-line-chart"></i> <span>Inventario</span>
              </a>
          </li>
        <li>
          <a href="{{URL::to('configuracions')}}">
            <i class="fa fa-th"></i> <span>Configuración</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">IVA</small>
            </span>
          </a>
        </li>
          <li>
              <a href="{{URL::to('users')}}">
                  <i class="fa fa-lock"></i> <span>Usuarios</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">Administrador</small>
            </span>
              </a>
          </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">

        <div id="morris-donut-chart"></div>
            @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Molinos el Zuliano Version</b> 1.0
    </div>
    <strong>Gustavo Gonzalez &copy; 2016</strong>
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
{!! Html::script('plugins/jQuery/jquery-2.2.3.min.js') !!}
<!-- jQuery UI 1.11.4 CDN -->
{{-- {!! Html::script('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') !!} --}}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
{!! Html::script('bootstrap/js/bootstrap.min.js') !!}
<!-- Morris.js charts-->

<script src="{{asset("plugins/morris/raphael.min.js")}}"></script>
<script src="{{asset("plugins/morris/morris.min.js")}}"></script>
<script>

    Morris.Donut({
        element: 'chart',
        data: [
            @if(isset($facturados))
            @foreach($facturados as $facturado)
                {label: '{{$facturado->descripcion}}', value: '{{$facturado->cantidad}}' },
            @endforeach
            @endif
        ],
        backgroundColor: '#ccc',
        labelColor: '#060',
        colors: [
            '#e21219',
            '#452ce8',
            '#328c0c',
            '#636661'
        ]
    });
</script>
<!-- datepicker -->
{!! Html::script('plugins/datepicker/bootstrap-datepicker.js') !!}
{!! Html::script('plugins/datatables/jquery.dataTables.js') !!}
<!-- AdminLTE App -->
{!! Html::script('dist/js/app.min.js') !!}
<!-- Table add row-->
{!! Html::script('dist/js/add.row.js') !!}
{{--
{!! Html::script('dist/js/add.row_product.js') !!}
--}}
{!! Html::script('plugins/select2/select2.full.min.js') !!}
{!! Html::script('dist/js/angular.js') !!}
{!! Html::script('dist/js/app.angular.js') !!}
{{-- input mask --}}
{!! Html::script('plugins/input-mask/jquery.inputmask.js') !!}
{!! Html::script('plugins/input-mask/jquery.inputmask.numeric.extensions.js') !!}
{!! Html::script('plugins/input-mask/jquery.inputmask.extensions.js') !!}
{{-- <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script> --}}
{!! Html::script('dist/js/angular-locale_de-de.js') !!}
{!! Html::script('dist/js/angular-input-decimal-separator.js') !!}
{!! Html::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
<script>
  $(function () {
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: "es",
      format: "yyyy-mm-dd",
      todayHighlight: true,
    });
    $('#datepicker2').datepicker({
      autoclose: true,
      language: "es",
      format: "yyyy-mm-dd",
      todayHighlight: true,
    });
    $('#example1').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": false
    });
   $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": false
    });
    $('#example3').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": false
    });
    $('#example4').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": false
    });
     $(":input").inputmask();
     $('#T1').wysihtml5({
       toolbar: {
          "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
          "emphasis": true, //Italics, bold, etc. Default true
          "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
          "html": false, //Button which allows you to edit the generated HTML. Default false
          "link": false, //Button to insert a link. Default true
          "image": false, //Button to insert an image. Default true,
          "color": false, //Button to change color of font
          "blockquote": false, //Blockquote

          //default: none, other options are xs, sm, lg
  }
     });


     $('#T2').wysihtml5();






  });
</script>
</body>
</html>
