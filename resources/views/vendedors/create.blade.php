@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Agregar vendedor</h1>
        </div>
    </div>

    <div class="row">
        {!! Form::open(['route' => 'vendedors.store']) !!}

            @include('vendedors.fields')

        {!! Form::close() !!}
    </div>
@endsection
