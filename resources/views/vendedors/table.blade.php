<div class="row">
    <section class="content-header">
        <h1>
            Administración de Vendedores
            <small>Todos los vendedores de Molinos del Zulia</small>
        </h1>

    </section>
    <br/><br/>


<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Clientes y proveedores</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                </div>

            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover" id="clientes-table">

                <thead>
                <th>Nombre</th>
                <th>Cédula de Identidad</th>
                <th>Teléfono de contacto</th>
                <th>Correo eléctronico</th>
                <th colspan="3">Acciones</th>
                </thead>

                <tbody>
                @foreach($vendedors as $vendedor)
                    <tr>
                        <td>{!! $vendedor->nombre !!}</td>
                        <td>{!! $vendedor->ci !!}</td>
                        <td>{!! $vendedor->telefono !!}</td>
                        <td>{!! $vendedor->email !!}</td>
                        <td>
                            {!! Form::open(['route' => ['vendedors.destroy', $vendedor->id], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                <a href="{!! route('vendedors.show', [$vendedor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="{!! route('vendedors.edit', [$vendedor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <!-- /.box-body -->
    </div>
    <a class="btn btn-success pull-left" href="{!! route('vendedors.create') !!}">Agregar vendedor</a>
    <!-- /.box -->
</div>


