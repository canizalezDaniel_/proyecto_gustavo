@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Información de vendedor
        </h1>
    </section>
    <br/>
    @include('vendedors.show_fields')

    <div class="form-group">
        <a class="btn btn-default btn-sm" href="javascript:history.go(-1)"><label class="fa fa-undo"></label> Atras!</a>

        {{--<a href="{!! route('vendedors.index') !!}" class="btn btn-default">Volver</a>--}}
    </div>
@endsection
