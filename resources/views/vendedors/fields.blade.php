<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Vendedor</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre y Apellido</label>
                {!! Form::text('nombre', null, ['class' => 'form-control']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputFile">Cédula de identidad o RIF</label>
                {!! Form::text('ci', null, ['class' => 'form-control', 'data-inputmask'=>"'mask': 'a-99999999-9', 'greedy': false"]) !!}
                <p class="help-block">RIF: J-99999999-9 ó CI:V-99999999-0</p>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Correo eléctronico</label>
                {!! Form::email('email', null, ['class' => 'form-control']) !!}

            </div>


            <div class="form-group">
                <label for="exampleInputPassword1">Teléfono contacto</label>
                {!! Form::text('telefono', null, ['class' => 'form-control', 'data-inputmask'=>"'mask': '+58(9999)-9999999', 'greedy': false"]) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Dirección</label>
                {!! Form::textarea('direccion', null, ['class' => 'form-control', 'cols'=>"5", 'rows'=>'2']) !!}


            </div>

            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('vendedors.index') !!}" class="btn btn-default">Cancelar</a>
        </div>
    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>


