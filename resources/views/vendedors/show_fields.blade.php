
<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Vendedor</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre y Apellido</label>
                {!! Form::text('nombre', $vendedor->nombre, ['class' => 'form-control', 'readonly']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputFile">Cédula o RIF</label>
                {!! Form::text('ci', $vendedor->ci, ['class' => 'form-control','readonly']) !!}


                <p class="help-block">RIF: J-99999999-9 ó CI:V-99999999-0</p>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Correo eléctronico</label>
                {!! Form::text('email', $vendedor->email, ['class' => 'form-control', 'readonly']) !!}

            </div>


            <div class="form-group">
                <label for="exampleInputPassword1">Teléfono contacto</label>
                {!! Form::text('telefono', $vendedor->telefono, ['class' => 'form-control', 'data-inputmask'=>"'mask': '+58(9999)-9999999', 'greedy': false", 'readonly']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Dirección</label>
                {!! Form::textarea('direccion', $vendedor->direccion, ['class' => 'form-control', 'cols'=>"5", 'rows'=>'2', 'readonly']) !!}


            </div>



        </div>
    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>



