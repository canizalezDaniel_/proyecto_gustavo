@extends('layouts.admin')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Modificar vendedor</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($vendedor, ['route' => ['vendedors.update', $vendedor->id], 'method' => 'patch']) !!}

            @include('vendedors.fields')

            {!! Form::close() !!}
        </div>
@endsection
