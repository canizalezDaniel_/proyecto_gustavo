@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Información de producto
        </h1>
    </section>
    <br/>
    @include('products.show_fields')

    <div class="form-group">
        <a class="btn btn-default btn-sm" href="javascript:history.go(-1)"><label class="fa fa-undo"></label> Atras!</a>

        {{--<a href="{!! route('products.index') !!}" class="btn btn-default">Atras</a>--}}
    </div>
@endsection
