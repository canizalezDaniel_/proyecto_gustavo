@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Agregar producto</h1>
        </div>
    </div>


    <div class="row">
        {!! Form::open(['route' => 'products.store', 'files' => true]) !!}

            @include('products.fields')

        {!! Form::close() !!}
    </div>
@endsection
