
<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Producto</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Código de producto</label>
                {!! Form::text('nombre_razon_social', $product->codigo, ['class' => 'form-control', 'readonly']) !!}

            </div>


            <div class="form-group">
                <label for="exampleInputPassword1">Descripción de producto</label>
                {!! Form::textarea('descripcion', $product->descripcion, ['class' => 'form-control', 'cols'=>"5", 'rows'=>'2', 'readonly']) !!}


            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Imágen de producto:</label>

                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="{{ asset('uploads/'.$product->foto) }}">
                    </div>

                </div>

            </div>



        </div>
    </div>
    <!-- /.box-body -->

    <!-- Submit Field -->
</div>