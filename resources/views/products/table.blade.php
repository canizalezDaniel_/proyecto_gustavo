<div class="row">

    <section class="content-header">
        <h1>
            Administración de Productos
            <small>Todos los productos hechos en Molinos del Zulia</small>
        </h1>

    </section>
    <br/><br/>

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Productos</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                    </div>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="clientes-table">

                    <thead>
                    <th>Código</th>
                    <th>Descripción</th>
                    <th colspan="3">Acciones</th>
                    </thead>

                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{!! $product->codigo !!}</td>
                            <td>{!! $product->descripcion !!}</td>
                            <td>
                                {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('products.show', [$product->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a href="{!! route('products.edit', [$product->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <!-- /.box-body -->
        </div>
        <a class="btn btn-success pull-left" href="{!! route('products.create') !!}">Agregar producto</a>

        <!-- /.box -->
    </div>
</div>



