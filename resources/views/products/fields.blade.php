<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Producto</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Código de producto</label>
                {!! Form::text('codigo', null, ['class' => 'form-control', 'data-inputmask'=>"'mask': '9-9999', 'greedy': false"]) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputFile">Descripción de producto</label>
                {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '5']) !!}
            </div>

            <div class="form-group">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="{{ asset('imagenes/preview.jpg') }}">
                    </div>
                    <div>


                            <span class="fileinput-new">Buscar imagen<input type="file" name="foto">

                    </div>
                </div>

            </div>



            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('products.index') !!}" class="btn btn-default">Cancelar</a>
        </div>
    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>



