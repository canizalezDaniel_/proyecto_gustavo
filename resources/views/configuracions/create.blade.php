@extends('layouts.admin')

@section('content')
    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'configuracions.store']) !!}

            @include('configuracions.fields')

        {!! Form::close() !!}
    </div>
@endsection
