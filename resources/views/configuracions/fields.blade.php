<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Configuración</h3>
            </div>
            <div class="box-body">

              <h3 class="box-title">IVA BS %</h3>

              <div class="input-group">

                <span class="input-group-addon">Bs</span>
                {!! Form::number('iva_bs', null, ['class' => 'form-control']) !!}
              </div>






<h3 class="box-title">Última factura impresa</h3>

<div class="input-group">
  <span class="input-group-addon">Nº</span>
  {!! Form::number('factura_codigo', null, ['class' => 'form-control', 'step'=> 'any']) !!}
</div>

<br>
              <div class="input-group">

                  {{ Form::button('<i class="fa fa-save"><span> Guardar </span> </i>', ['type' => 'submit', 'class' => 'btn btn-success btn-lg'] )  }}

                  <a href="{!! route('configuracions.index') !!}" class="btn btn-danger  btn-lg"> <i class="fa fa-undo"></i> <span>Cancelar</span></a>


              </div>


                <!-- /.col-lg-6 -->
              </div>
              <!-- /.row -->


            </div>

            <!-- /.box-body -->
