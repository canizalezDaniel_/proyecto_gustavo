@extends('layouts.admin')

@section('content')
    @include('configuracions.show_fields')

    <div class="form-group">
           <a href="{!! route('configuracions.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
