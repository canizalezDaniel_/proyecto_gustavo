@extends('layouts.admin')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Editar valores de configuración</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
          <div class="col-md-12">
            {!! Form::model($configuracion, ['route' => ['configuracions.update', $configuracion->id], 'method' => 'patch']) !!}

            @include('configuracions.fields')

            {!! Form::close() !!}
          </div>
        </div>
@endsection
