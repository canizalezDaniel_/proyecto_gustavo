

    @foreach($configuracions as $configuracion)
        <div class="col-lg-3 col-xs-8">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3> <sup style="font-size: 20px">Nº</sup> {!! $configuracion->nota_debito_codigo !!} </h3>

              <p>NOTA DÉBITO</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-square-o"></i>
            </div>
            <a href="{!! route('configuracions.edit', [$configuracion->id]) !!}" class="small-box-footer">
              Configurar <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-8">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><sup style="font-size: 20px">Nº</sup> {!! $configuracion->nota_credito_codigo !!}</h3>

              <p>NOTA DE CRÉDITO</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-square-o"></i>
            </div>
            <a href="{!! route('configuracions.edit', [$configuracion->id]) !!}" class="small-box-footer">
              Configurar <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
@endforeach
    
 
