

    @foreach($configuracions as $configuracion)

        <!-- ./col -->
        <div class="col-lg-3 col-xs-8">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{!! $configuracion->iva_bs !!}<sup style="font-size: 20px">%</sup></h3>

              <p>IVA Bs</p>
            </div>
            <div class="icon">
              <i class="fa fa-percent"></i>
            </div>
            <a href="{!! route('configuracions.edit', [$configuracion->id]) !!}" class="small-box-footer">
              Configurar <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-8">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><sup style="font-size: 20px">Nº</sup>  {{$configuracion->factura_codigo}} </h3>

                    <p>FACTURA</p>
                </div>
                <div class="icon">
                    <i class="fa fa-check-square-o"></i>
                </div>
                <a href="{!! route('configuracions.edit', [$configuracion->id]) !!}" class="small-box-footer">
                    Configurar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
@endforeach
     