<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $configuracion->id !!}</p>
</div>

<!-- Iva Usd Field -->
<div class="form-group">
    {!! Form::label('iva_usd', 'Iva Usd:') !!}
    <p>{!! $configuracion->iva_usd !!}</p>
</div>

<!-- Iva Bs Field -->
<div class="form-group">
    {!! Form::label('iva_bs', 'Iva Bs:') !!}
    <p>{!! $configuracion->iva_bs !!}</p>
</div>

<!-- Tasa Usd Field -->
<div class="form-group">
    {!! Form::label('tasa_usd', 'Tasa Usd:') !!}
    <p>{!! $configuracion->tasa_usd !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $configuracion->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $configuracion->updated_at !!}</p>
</div>

