@extends('layouts.admin')

@section('content')
  {{-- <h1 class="pull-left">Configuraciones de IVA</h1> --}}
  {{-- <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('configuracions.create') !!}">Add New</a> --}}

  <div class="clearfix"></div>

  @include('flash::message')

  
<div class="row">
   @include('configuracions.table')
</div>


   {{--  <div class="col-md-10">
      <h2 class="box-title">Administración de IVA/TASA DE CAMBIO</h2>
      @include('configuracions.table')
    </div>
    <div class="col-md-10">
      <h2 class="box-title">Ultima configuración de consecutivos</h2>
      @include('configuracions.table_consecutivos')
    </div> --}}
@endsection
