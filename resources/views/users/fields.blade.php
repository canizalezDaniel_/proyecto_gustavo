
<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Usuarios de Molinos del Zulia</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre y Apellido:</label>
                {!! Form::text('name', null, ['class' => 'form-control']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputFile">Teléfono contacto:</label>
                {!! Form::text('telefono', null, ['class' => 'form-control', 'data-inputmask'=>"'mask': '+58(9999)-9999999', 'greedy': false"]) !!}


                <p class="help-block">Formato: +58(9999)-9999999</p>
            </div>


            <div class="form-group">
                <label for="exampleInputPassword1">Correo eléctronico:</label>
                {!! Form::email('email', null, ['class' => 'form-control']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Clave de acceso:</label>
                {{ Form::password('password', array('class' => 'form-control')) }}

            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Confirmar clave de acceso:</label>
                {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

            </div>

            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('users.index') !!}" class="btn btn-default">Cancelar</a>
        </div>
    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>
