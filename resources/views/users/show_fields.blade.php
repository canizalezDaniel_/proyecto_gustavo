
<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Usuario</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre y Apellido:</label>
                {!! Form::text('name', $user->name, ['class' => 'form-control', 'readonly']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputFile">Teléfono contacto:</label>
                {!! Form::text('telefono', $user->telefono, ['class' => 'form-control','readonly']) !!}
            </div>


            <div class="form-group">
                <label for="exampleInputPassword1">Correo eléctronico:</label>
                {!! Form::email('email', $user->email, ['class' => 'form-control', 'readonly']) !!}

            </div>



        </div>
    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>

