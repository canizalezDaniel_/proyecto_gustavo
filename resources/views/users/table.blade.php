<div class="row">

    <section class="content-header">
        <h1>
            Administración de Usuarios
            <small>Todos los usuarios que pueden ingresar a Molinos del Zulia</small>
        </h1>

    </section>
    <br/><br/>

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Usuarios</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                    </div>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="clientes-table">

                    <thead>
                    <th>Name</th>
                    <th>Telefono</th>
                    <th>Email</th>
                    <th colspan="3">Acciones</th>
                    </thead>

                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{!! $user->name !!}</td>
                            <td>{!! $user->telefono !!}</td>
                            <td>{!! $user->email !!}</td>

                            <td>
                                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <!-- /.box-body -->
        </div>
        <a class="btn btn-success pull-left" href="{!! route('users.create') !!}">Nuevo usuario</a>

        <!-- /.box -->
    </div>
</div>



