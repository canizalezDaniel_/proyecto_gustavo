@extends('layouts.admin')

 @section('content')


 <div class="row">
               
            <div class="col-md-10 col-md-offset-2">
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <center><img src="imagenes/nota_entrega2.png" class="img-thumbnail" width="180" height="180"></center>
                            </div>
                        </div>
                        <a href="{{URL::to('notasEntregas/create')}}">
                            <div class="panel-footer">
                               <center><button class="btn btn-danger">NOTAS DE ENTREGA</button></center>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <center><img src="imagenes/nota_debito.png" class="img-thumbnail" width="180" height="180"></center>
                            </div>
                        </div>
                        <a href="{{URL::to('notasDebitos/create')}}">
                            <div class="panel-footer">
                               <center><button class="btn btn-danger">NOTAS DE DÉBITO</button></center>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <center><img src="imagenes/nota_credito.png" class="img-thumbnail" width="180" height="180"></center>
                            </div>
                        </div>
                        <a href="{{URL::to('notasCreditos/create')}}">
                            <div class="panel-footer">
                               <center><button class="btn btn-danger">NOTAS DE CRÉDITO</button></center>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

@endsection
