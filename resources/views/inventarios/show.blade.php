@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Información de producto
        </h1>
    </section>
    <br/>
    @include('inventarios.show_fields')

    <div class="form-group">
        <a class="btn btn-default btn-sm" href="javascript:history.go(-1)"><label class="fa fa-undo"></label> Atras!</a>
    </div>
@endsection
