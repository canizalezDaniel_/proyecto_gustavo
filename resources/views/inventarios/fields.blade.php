
<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Inventario</h3>
        </div>
        <div class="box-body">
            @if(isset($productos))
                <div class="form-group">
                    <label for="">Producto:</label>
                    <select class="form-control input-sm" name="productos_id">
                        <option value="">Seleccionar un producto</option>
                        @foreach($productos as $producto)
                            <option value="{{$producto->id}}">{{$producto->codigo}} - {{$producto->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            <div class="form-group">
                <label for="exampleInputFile">Cantidad en inventario:</label>
                {!! Form::number('cantidad', null, ['class' => 'form-control']) !!}
            </div>


            <div class="form-group">
                <label for="exampleInputPassword1">Precio de producto:</label>
                {!! Form::text('precio', null, ['class' => 'form-control', 'decimal-min'=>'0', 'input-decimal-separator'=>'3']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Fecha de entrada:</label>
                {!! Form::text('fecha_entrada', Carbon\Carbon::today()->toDateString(), ['class' => 'form-control input-sm', 'placeholder'=>'Fecha', 'id'=> 'datepicker', 'required']) !!}


            </div>

            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('inventarios.index') !!}" class="btn btn-default">Cancelar</a>
        </div>
    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>