<div class="row">

    <section class="content-header">
        <h1>
            Administración de Inventario
            <small>Todos los productos de Molinos del Zulia</small>
        </h1>

    </section>
    <br/><br/>

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Inventario de productos</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                    </div>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="clientes-table">

                    <thead>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Fecha Entrada</th>
                    <th colspan="3">Acciones</th>
                    </thead>

                    <tbody>
                    @foreach($inventarios as $inventario)
                        <tr>
                            <td>{!! $inventario->codigo !!} - {!! $inventario->descripcion !!}</td>
                            <td>{!! $inventario->cantidad !!}</td>
                            <td>{!! $inventario->precio !!}</td>
                            <td>{!! $inventario->fecha_entrada !!}</td>
                            <td>
                                {!! Form::open(['route' => ['inventarios.destroy', $inventario->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('inventarios.show', [$inventario->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a href="{!! route('inventarios.edit', [$inventario->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                    <a href="{!! route('inventarios.upload', [$inventario->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-upload"></i></a>
                                    <a href="{!! route('inventarios.download', [$inventario->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-download"></i></a>

                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <!-- /.box-body -->
        </div>
        <a class="btn btn-success pull-left" href="{!! route('inventarios.create') !!}">Agregar inventario</a>

        <!-- /.box -->
    </div>
</div>


