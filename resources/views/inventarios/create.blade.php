@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Agregar producto en inventario</h1>
        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'inventarios.store']) !!}

            @include('inventarios.fields')

        {!! Form::close() !!}
    </div>
@endsection
