
<div class="form-group col-sm-12">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Inventario</h3>
        </div>
        <div class="box-body">
             @foreach($inventario as $inventario)
            <div class="form-group">
                <label for="exampleInputEmail1">Código:</label>
                {!! Form::text('codigo', $inventario->codigo, ['class' => 'form-control', 'readonly']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Producto:</label>
                {!! Form::text('descripcion', $inventario->descripcion, ['class' => 'form-control', 'readonly']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputFile">Cantidad en inventario:</label>
                {!! Form::text('cantidad', $inventario->cantidad, ['class' => 'form-control', 'readonly']) !!}

            </div>



            <div class="form-group">
                <label for="exampleInputPassword1">Precio de producto:</label>
                {!! Form::text('precio', $inventario->precio, ['class' => 'form-control', 'data-inputmask'=>"'mask': '+58(9999)-9999999', 'greedy': false", 'readonly']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Dirección Fiscal</label>
                {!! Form::text('fecha_entrada', $inventario->fecha_entrada, ['class' => 'form-control', 'readonly']) !!}


            </div>


            @endforeach


        </div>

    </div>
    <!-- /.box-body -->


    <!-- Submit Field -->



</div>


