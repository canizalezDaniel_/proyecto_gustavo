@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if($estatus == '1')
            <h1 class="pull-left">Carga de productos en inventario</h1>
                @else
            <h1 class="pull-left">Descarga de productos en inventario</h1>
            @endif

        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'inventarios.upload_post']) !!}
        <div class="form-group col-sm-12">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Inventario</h3>
                </div>
            @foreach($producto as $producto)
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputFile">Producto:</label>
                        {!! Form::text('codigo', $producto->codigo." - ".$producto->descripcion, ['class' => 'form-control', 'readonly']) !!}
                        {!! Form::text('id', $producto->id, ['class' => 'form-control']) !!}
                        {!! Form::hidden('productos_id', $producto->productos_id, ['class' => 'form-control']) !!}
                        {!! Form::hidden('users_id', Auth::user()->id , ['class' => 'form-control']) !!}
                        <div class="alert alert-warning">
                            <strong>En inventario:</strong> {{$producto->cantidad}}.
                        </div>
                    </div>
            @endforeach
                    <div class="form-group">
                        <label for="exampleInputFile">Cantidad:</label>
                        {!! Form::number('cantidad', null, ['class' => 'form-control']) !!}
                    </div>

                    {{--Carga de productos--}}
                    @if($estatus == '1')
                    <div class="form-group">
                        {!! Form::hidden('estatus', "1", ['class' => 'form-control']) !!}

                        <label for="exampleInputFile">Motivo de carga:</label>
                        <select name="motivo" class="form-control">
                            <option value="1">Producción</option>
                            <option value="2">Devolución</option>
                        </select>
                    </div>
                    @else
                        {{--Descarga de productos--}}
                        <div class="form-group">
                            {!! Form::hidden('estatus', "2", ['class' => 'form-control']) !!}

                            <label for="exampleInputFile">Motivo de descarga:</label>
                            <select name="motivo" class="form-control">
                                <option value="1">Avería</option>
                                <option value="2">Pérdida</option>
                                <option value="3">Vencimiento</option>

                            </select>
                        </div>

                    @endif


                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                    <a href="{!! route('inventarios.index') !!}" class="btn btn-default">Cancelar</a>
                </div>
            </div>
            <!-- /.box-body -->


            <!-- Submit Field -->



        </div>
        {!! Form::close() !!}
    </div>
@endsection
