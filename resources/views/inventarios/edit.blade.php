@extends('layouts.admin')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Modificar inventario</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($inventario, ['route' => ['inventarios.update', $inventario->id], 'method' => 'patch']) !!}

            @include('inventarios.fields')

            {!! Form::close() !!}
        </div>
@endsection
