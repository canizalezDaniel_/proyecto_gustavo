-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando datos para la tabla proyecto_gustavo.clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`id`, `nombre_razon_social`, `direccion_fiscal`, `ci_rif`, `ciudad`, `estado`, `telefono`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Tienda Latino Chong', 'Maracaibo. Zulia', 'E-11589881-3', 'Maracaibo', 'Zulia', '+58(0458)-9599666', '2017-07-19 20:28:14', '2017-07-19 20:28:14', NULL),
	(2, 'Charcuteria Jairo C.A', 'San Francisco. Sierra Maestra', 'J-30003883-5', '', '', '+58(1258)-8936996', '2017-11-29 23:16:07', '2017-11-29 23:16:07', NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.configuracions: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `configuracions` DISABLE KEYS */;
INSERT INTO `configuracions` (`id`, `iva_usd`, `iva_bs`, `tasa_usd`, `nota_debito_codigo`, `nota_credito_codigo`, `factura_codigo`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 0, 12, 0.00, 0, 0, 0, '2017-11-28 23:14:05', '2017-11-29 23:12:55', NULL);
/*!40000 ALTER TABLE `configuracions` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.facturas: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` (`id`, `lugar_emision`, `fecha_emision`, `factura_codigo`, `nota_codigo`, `condicion_pago`, `credito_condicion_dias`, `cheque_numero`, `banco_cuenta`, `vencimiento`, `forma_pago`, `credito_dias`, `estatus`, `total_base`, `ajustes`, `iva_bs`, `total_bs`, `vendedors_id`, `clientes_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Maracaibo', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 1, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(2, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 1, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(3, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 3, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(4, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 1, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(5, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 3, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(6, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 1, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(7, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 2, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(8, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 3, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(9, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 2, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL),
	(10, 'San Francisco', '2017-11-29', '000001', '0002', 'Contado', NULL, NULL, NULL, '2017-11-29', 'Efectivo', NULL, 1, '1.400,00', '0', '168,00', '1.568,00', 1, 1, '2017-11-29 20:53:25', '2017-11-29 22:34:46', NULL);
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.inventarios: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `inventarios` DISABLE KEYS */;
INSERT INTO `inventarios` (`id`, `productos_id`, `cantidad`, `precio`, `fecha_entrada`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 0, 700, '2017-07-15', '2017-07-15 19:51:45', '2017-11-29 20:53:25', NULL),
	(2, 2, 12, 900, '2017-07-15', '2017-07-15 19:51:55', '2017-07-15 19:51:55', NULL),
	(3, 3, 15, 1200, '2017-07-15', '2017-07-15 19:52:10', '2017-07-15 19:52:10', NULL),
	(4, 5, 160, 160000, '2017-11-29', '2017-11-29 23:14:39', '2017-11-29 23:14:39', NULL);
/*!40000 ALTER TABLE `inventarios` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.items_facturas: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `items_facturas` DISABLE KEYS */;
INSERT INTO `items_facturas` (`id`, `cantidad`, `descripcion`, `precio_unitario_bs`, `total_item_bs`, `facturas_id`, `productos_id`, `created_at`, `updated_at`) VALUES
	(1, 2, '', '700', '1.400,00', 1, 1, '2017-11-29 20:53:25', '2017-11-29 20:53:25'),
	(2, 3, '0', '800', '1.500,00', 1, 2, '2017-12-02 20:56:28', '2017-12-02 20:56:29'),
	(3, 5, '0', '900', '1.800,00', 1, 3, '2017-12-02 20:56:51', '2017-12-02 20:56:52'),
	(4, 6, '0', '1100', '2.800,00', 1, 1, '2017-12-02 20:57:10', '2017-12-02 20:57:11'),
	(5, 5, '0', '900', '1.800,00', 5, 3, '2017-12-02 20:56:51', '2017-12-02 20:56:52'),
	(6, 5, '0', '900', '1.800,00', 5, 3, '2017-12-02 20:56:51', '2017-12-02 20:56:52'),
	(7, 5, '0', '900', '1.800,00', 1, 3, '2017-12-02 20:56:51', '2017-12-02 20:56:52'),
	(8, 5, '0', '900', '1.800,00', 6, 3, '2017-12-02 20:56:51', '2017-12-02 20:56:52'),
	(9, 5, '0', '900', '1.800,00', 6, 3, '2017-12-02 20:56:51', '2017-12-02 20:56:52');
/*!40000 ALTER TABLE `items_facturas` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.items_notas_creditos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `items_notas_creditos` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_notas_creditos` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.items_notas_debitos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `items_notas_debitos` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_notas_debitos` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.items_notas_entregas: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `items_notas_entregas` DISABLE KEYS */;
INSERT INTO `items_notas_entregas` (`id`, `item`, `productos_id`, `observaciones`, `unidad`, `cantidad`, `cantidad_entregada`, `notas_entrega_id`, `created_at`, `updated_at`) VALUES
	(3, 1, 4, '', '', 200, 0, 7, '2017-11-27 22:20:04', '2017-11-27 22:20:04'),
	(4, 1, 5, '', '', 600, 0, 8, '2017-11-29 23:20:04', '2017-11-29 23:20:04'),
	(5, 2, 4, '', '', 100, 0, 8, '2017-11-29 23:20:04', '2017-11-29 23:20:04'),
	(6, 3, 1, '', '', 10, 0, 8, '2017-11-29 23:20:04', '2017-11-29 23:20:04');
/*!40000 ALTER TABLE `items_notas_entregas` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.migrations: ~17 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2014_10_12_000000_create_users_table', 1),
	('2014_10_12_100000_create_password_resets_table', 1),
	('2016_09_08_171513_create_notas_entregas_table', 1),
	('2016_09_08_174340_create_notas_debitos_table', 1),
	('2016_09_08_175639_create_notas_creditos_table', 1),
	('2016_09_08_180534_create_vendedors_table', 1),
	('2016_09_08_180535_create_clientes_table', 1),
	('2016_09_08_180536_create_facturas_table', 1),
	('2016_09_16_221932_create_items_notas_entregas_table', 1),
	('2016_09_16_222019_create_items_notas_debitos_table', 1),
	('2016_09_16_222035_create_items_notas_creditos_table', 1),
	('2016_09_16_222048_create_products_table', 1),
	('2016_09_16_222049_create_items_notas_facturas_table', 1),
	('2016_09_23_191558_create_configuracions_table', 1),
	('2017_06_20_235004_create_inventarios_table', 1),
	('2017_06_25_131752_create_motivoCarga_table', 1),
	('2017_06_25_131754_create_movimientoInventario_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.motivos_carga: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `motivos_carga` DISABLE KEYS */;
/*!40000 ALTER TABLE `motivos_carga` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.movimiento_inventario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `movimiento_inventario` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimiento_inventario` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.notas_creditos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `notas_creditos` DISABLE KEYS */;
/*!40000 ALTER TABLE `notas_creditos` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.notas_debitos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `notas_debitos` DISABLE KEYS */;
/*!40000 ALTER TABLE `notas_debitos` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.notas_entregas: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `notas_entregas` DISABLE KEYS */;
INSERT INTO `notas_entregas` (`id`, `lugar_emision`, `empresa_nombre`, `empresa_direccion`, `fecha_emision`, `ci_rif`, `telefono`, `nota_codigo`, `recibido_por`, `nombre_transportista`, `cedula_transportista`, `empresa_transportista`, `vehiculo_empresa`, `marca_vehiculo`, `placa_vehiculo`, `observaciones_nota`, `estatus`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(4, 'Maracaibo', '1', 'Maracaibo. Zulia', '2017-11-28', 'E-11589881-3', '+58(0458)-9599666', '0001', 'Daniel Canizalez', 'Tealca Conductor', 'J303458978-2', 'TEALCA', 'N/A', 'N/A', 'N/A', 'TEALCA', 1, '2017-11-27 22:07:05', '2017-11-27 22:14:19', '2017-11-27 22:14:19'),
	(5, 'Maracaibo', '1', 'Maracaibo. Zulia', '2017-11-28', 'E-11589881-3', '+58(0458)-9599666', '0001', 'Daniel Canizalez', 'Tealca Conductor', 'J303458978-2', 'TEALCA', 'N/A', 'N/A', 'N/A', 'BOA', 1, '2017-11-27 22:17:10', '2017-11-27 22:20:19', '2017-11-27 22:20:19'),
	(6, 'Maracaibo', '1', 'Maracaibo. Zulia', '2017-11-27', 'E-11589881-3', '+58(0458)-9599666', '0002', 'Daniel Canizalez', 'Tealca Conductor', 'J303458978-2', 'TEALCA', 'N/A', 'N/A', 'N/A', 'BOA', 1, '2017-11-27 22:19:41', '2017-11-27 22:20:23', '2017-11-27 22:20:23'),
	(7, 'Maracaibo', '1', 'Maracaibo. Zulia', '2017-11-27', 'E-11589881-3', '+58(0458)-9599666', '0002', 'Daniel Canizalez', 'Tealca Conductor', 'J303458978-2', 'TEALCA', 'N/A', 'N/A', 'N/A', 'BOA', 0, '2017-11-27 22:20:04', '2017-11-29 23:12:36', '2017-11-29 23:12:36'),
	(8, 'San Francisco', '2', 'San Francisco. Sierra Maestra', '2017-11-29', 'J-30003883-5', '+58(1258)-8936996', '0001', 'Gustavo Galue', '', '', '', '', '', '', '', 1, '2017-11-29 23:20:04', '2017-11-29 23:20:04', NULL);
/*!40000 ALTER TABLE `notas_entregas` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.products: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `codigo`, `descripcion`, `foto`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '0-0001', 'Salsa de tomate 12x24', '', '2017-07-15 19:50:48', '2017-07-15 19:50:48', NULL),
	(2, '0-0002', 'Salsa de Soya 12x24', '', '2017-07-15 19:51:09', '2017-07-15 19:51:09', NULL),
	(3, '0-0003', 'Mayonesa de galón 3x32', '', '2017-07-15 19:51:27', '2017-07-15 19:51:27', NULL),
	(4, '0-0009', 'Salsa Tomate Ketchun x12', 'salsa.jpg', '2017-07-19 21:01:59', '2017-07-19 21:01:59', NULL),
	(5, '0-0010', 'Salsa de soya 32x1 Kg Roja', '', '2017-11-29 23:13:57', '2017-11-29 23:13:57', NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `telefono`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Administrador', '04120645899', 'gustavo2388@gmail.com', '$2y$10$6rIIAUx0kZICrKLM2iJ9ReOBe..VUr.jWUO6hOMglW8YZQi3HaaLW', 'ibLj3EjJqM', '2017-11-28 23:14:05', '2017-11-28 23:14:05', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando datos para la tabla proyecto_gustavo.vendedors: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `vendedors` DISABLE KEYS */;
INSERT INTO `vendedors` (`id`, `nombre`, `ci`, `direccion`, `telefono`, `email`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Daniel Canizalez', 'V-18998885-0', 'AV 16 calle 14 Sierra Maestra Casa #14-28', '+58(0458)-99969', 'canizalezdaniel@gmail.com', '2017-07-19 20:28:41', '2017-07-19 20:28:41', NULL),
	(2, 'Jose Albornoz', 'V-18899699-0', 'Maracaibo', '+58(0415)-66589', 'jalbornoz@molinozulia.com', '2017-11-29 23:15:30', '2017-11-29 23:15:30', NULL),
	(3, 'Pedro Camejo', 'V-18899999-9', 'ASdadsaaaa', '+58--0000-6666', 'f@gmail.com', '2017-12-02 21:54:38', '2017-12-02 21:54:38', NULL);
/*!40000 ALTER TABLE `vendedors` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
