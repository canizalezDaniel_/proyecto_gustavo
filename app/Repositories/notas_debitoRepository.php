<?php

namespace App\Repositories;

use App\Models\notas_debito;
use InfyOm\Generator\Common\BaseRepository;

class notas_debitoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'lugar_emision',
        'empresa_nombre',
        'empresa_direccion',
        'fecha_emision',
        'ci_rif',
        'telefono',
        'nota_debito_codigo',
        'recibido_conforme'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return notas_debito::class;
    }
}
