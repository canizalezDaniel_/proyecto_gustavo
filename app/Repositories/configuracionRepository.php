<?php

namespace App\Repositories;

use App\Models\configuracion;
use InfyOm\Generator\Common\BaseRepository;

class configuracionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'iva_usd',
        'iva_bs',
        'tasa_usd'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return configuracion::class;
    }
}
