<?php

namespace App\Repositories;

use App\Models\notas_entrega;
use InfyOm\Generator\Common\BaseRepository;

class notas_entregaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'lugar_emision',
        'empresa_nombre',
        'empresa_direccion',
        'fecha_emision',
        'ci_rif',
        'telefono',
        'nota_codigo',
        'recibido_por',
        'nombre_transportista',
        'empresa_transportista',
        'vehiculo_empresa',
        'placa_vehiculo',
        'marca_vehiculo',
        'observaciones'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return notas_entrega::class;
    }
}
