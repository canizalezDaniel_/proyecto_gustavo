<?php

namespace App\Repositories;

use App\Models\factura;
use InfyOm\Generator\Common\BaseRepository;

class facturaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'razon_social',
        'direccion_fiscal',
        'ci_rif',
        'fecha_emision',
        'telefono',
        'nota_entrega_codigo',
        'condicion_pago',
        'dias',
        'forma_pago',
        'numero_cheque',
        'banco',
        'credito_dias'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return factura::class;
    }
}
