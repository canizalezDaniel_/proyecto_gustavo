<?php

namespace App\Repositories;

use App\Models\inventario;
use InfyOm\Generator\Common\BaseRepository;

class inventarioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fk_id_products',
        'cantidad',
        'precio',
        'fecha_entrada'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return inventario::class;
    }
}
