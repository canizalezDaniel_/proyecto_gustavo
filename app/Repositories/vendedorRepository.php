<?php

namespace App\Repositories;

use App\Models\vendedor;
use InfyOm\Generator\Common\BaseRepository;

class vendedorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ci',
        'direccion',
        'telefono',
        'email'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return vendedor::class;
    }
}
