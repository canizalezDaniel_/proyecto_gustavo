<?php

namespace App\Repositories;

use App\Models\notas_credito;
use InfyOm\Generator\Common\BaseRepository;

class notas_creditoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'lugar_emision',
        'empresa_nombre',
        'empresa_direccion',
        'fecha_emision',
        'ci_rif',
        'telefono',
        'nota_credito_codigo',
        'recibido_conforme'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return notas_credito::class;
    }
}
