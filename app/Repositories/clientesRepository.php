<?php

namespace App\Repositories;

use App\Models\clientes;
use InfyOm\Generator\Common\BaseRepository;

class clientesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_razon_social',
        'direccion_fiscal',
        'ci_rif',
        'telefono'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return clientes::class;
    }
}
