<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class vendedor
 * @package App\Models
 * @version June 18, 2017, 10:43 am VET
 */
class vendedor extends Model
{
    use SoftDeletes;

    public $table = 'vendedors';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'ci',
        'direccion',
        'telefono',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'ci' => 'string',
        'direccion' => 'string',
        'telefono' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'ci' => 'required',
        'direccion' => 'required',
        'email' => 'email'
    ];

    
}
