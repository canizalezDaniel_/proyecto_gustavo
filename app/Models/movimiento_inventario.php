<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class movimiento_inventario extends Model
{
    public $table = 'movimiento_inventario';

    //protected $dates = ['deleted_at'];

    public $fillable = [
        'productos_id',
        'users_id',
        'estatus',
        'cantidad',
        'motivo'
    ];
}
