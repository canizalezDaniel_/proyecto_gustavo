<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class configuracion
 * @package App\Models
 * @version September 23, 2016, 7:15 pm VET
 */
class configuracion extends Model
{
    use SoftDeletes;

    public $table = 'configuracions';


    protected $dates = ['deleted_at'];


    public $fillable = [

        'iva_bs',
        'factura_codigo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'iva_bs' => 'integer',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'iva_bs' => 'required',

    ];


}
