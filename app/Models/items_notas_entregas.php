<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class items_notas_entregas extends Model
{
    //

    public $fillable = [
        'item',
        'producto',
        'cantidad',
        
    ];

    public function notas_entrega()
    {
        return $this->belongsTo('App\Models\notas_entrega');
    }
}
