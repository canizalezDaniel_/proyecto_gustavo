<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class notas_entrega
 * @package App\Models
 * @version September 8, 2016, 5:15 pm VET
 */
class notas_entrega extends Model
{
    use SoftDeletes;

    public $table = 'notas_entregas';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'lugar_emision',
        'empresa_nombre',
        'empresa_direccion',
        'fecha_emision',
        'ci_rif',
        'telefono',
        'nota_codigo',
        'recibido_por',
        'nombre_transportista',
        'cedula_transportista',
        'empresa_transportista',
        'vehiculo_empresa',
        'placa_vehiculo',
        'marca_vehiculo',
        'observaciones_nota'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'lugar_emision' => 'string',
        'empresa_nombre' => 'string',
        'empresa_direccion' => 'string',
        'fecha_emision' => 'date',
        'ci_rif' => 'string',
        'telefono' => 'string',
        'nota_codigo' => 'string',
        'recibido_por' => 'string',
        'nombre_transportista' => 'string',
        'empresa_transportista' => 'string',
        'vehiculo_empresa' => 'string',
        'placa_vehiculo' => 'string',
        'marca_vehiculo' => 'string',
        'observaciones_nota' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'lugar_emision' => 'required',
        'empresa_nombre' => 'required',
        'empresa_direccion' => 'required',
        'fecha_emision' => 'required',
        'ci_rif' => 'required',
        'telefono' => 'required',
        'nota_codigo' => 'required',
        'placa_vehiculo' => 'required',
        'marca_vehiculo' => 'required'
    ];

    public function items()
    {
        return $this->hasMany('App\Models\items_notas_entregas');
    }


}
