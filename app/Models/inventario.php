<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class inventario
 * @package App\Models
 * @version June 20, 2017, 11:50 pm VET
 */
class inventario extends Model
{
    use SoftDeletes;

    public $table = 'inventarios';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'productos_id',
        'cantidad',
        'precio',
        'fecha_entrada'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'productos_id' => 'integer',
        'cantidad' => 'integer',
        'precio' => 'double',
        'fecha_entrada' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cantidad' => 'required',
        'precio' => 'required',
        'fecha_entrada' => 'required'
    ];

    
}
