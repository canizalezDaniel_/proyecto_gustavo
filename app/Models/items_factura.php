<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class items_factura extends Model
{
    //
    public function facturas()
    {
        return $this->belongsTo('App\Models\factura');
    }
}
