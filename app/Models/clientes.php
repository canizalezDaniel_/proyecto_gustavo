<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class clientes
 * @package App\Models
 * @version September 8, 2016, 6:05 pm VET
 */
class clientes extends Model
{
    use SoftDeletes;

    public $table = 'clientes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre_razon_social',
        'direccion_fiscal',
        'ci_rif',
        'telefono'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre_razon_social' => 'string',
        'direccion_fiscal' => 'string',
        'ci_rif' => 'string',
        'telefono' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_razon_social' => 'required',
        'direccion_fiscal' => 'required',
        'ci_rif' => 'required',
        'telefono' => 'required'
    ];

    
}
