<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class factura
 * @package App\Models
 * @version September 8, 2016, 6:04 pm VET
 */
class factura extends Model
{
    use SoftDeletes;

    public $table = 'facturas';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'lugar_emision',
        'vendedors_id',
        'clientes_id',
        'fecha_emision',
        'vencimiento',
        'nota_codigo',
        'factura_codigo',
        'nota_codigo',
        'condicion_pago',
        'credito_condicion_dias',
        'dias_credito',
        'forma_pago',
        'credito_dias',
        'cheque_numero',
        'banco_cuenta',
        'total_base',
        'ajustes',
        'iva_bs',
        'total_bs'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'fecha_emision' => 'date',
        'telefono' => 'string',
        'nota_entrega_codigo' => 'string',
        'forma_pago' => 'string',
        'numero_cheque' => 'string',
        'banco_cuenta' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

        'fecha_emision' => 'required',
        'condicion_pago' => 'required',
        'forma_pago' => 'required'
    ];

    public function items()
    {
        return $this->hasMany('App\Models\items_factura');
    }


}
