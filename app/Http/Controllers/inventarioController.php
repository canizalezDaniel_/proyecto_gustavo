<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateinventarioRequest;
use App\Http\Requests\UpdateinventarioRequest;
use App\Models\inventario;
use App\Models\Product;
use App\Models\movimiento_inventario;
use App\Repositories\inventarioRepository;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class inventarioController extends BaseController
{
    /** @var  inventarioRepository */
    private $inventarioRepository;

    public function __construct(inventarioRepository $inventarioRepo)
    {
        $this->inventarioRepository = $inventarioRepo;
    }

    /**
     * Display a listing of the inventario.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inventarioRepository->pushCriteria(new RequestCriteria($request));
        //$inventarios = $this->inventarioRepository->all();
        $inventarios = DB::table('inventarios')
            ->join('products', 'products.id', '=', 'inventarios.productos_id')
            ->select('inventarios.id','products.codigo', 'products.descripcion', 'inventarios.fecha_entrada', 'inventarios.cantidad', 'inventarios.precio')
            ->get();
        //dd($inventarios);
        return view('inventarios.index')
            ->with('inventarios', $inventarios);
    }

    /**
     * Show the form for creating a new inventario.
     *
     * @return Response
     */
    public function create()
    {
        $productos = Product::all(['id', 'codigo', 'descripcion']);
        return view('inventarios.create')->with('productos', $productos);
    }

    /**
     * Store a newly created inventario in storage.
     *
     * @param CreateinventarioRequest $request
     *
     * @return Response
     */
    public function store(CreateinventarioRequest $request)
    {
        $input = $request->all();

        $inventario = $this->inventarioRepository->create($input);

        Flash::success('Producto registrado en inventario con exito.');

        return redirect(route('inventarios.index'));
    }

    /**
     * Display the specified inventario.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$inventario = $this->inventarioRepository->findWithoutFail($id);
        $inventario = DB::table('inventarios')
            ->join('products', 'products.id', '=', 'inventarios.productos_id')
            ->select('inventarios.id','products.codigo', 'products.descripcion', 'inventarios.fecha_entrada', 'inventarios.cantidad', 'inventarios.precio')
            ->where('products.id', $id)
            ->get();

        if (empty($inventario)) {
            Flash::error('Inventario not found');

            return redirect(route('inventarios.index'));
        }

        return view('inventarios.show')->with('inventario', $inventario);
    }

    /**
     * Show the form for editing the specified inventario.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
       $inventario = $this->inventarioRepository->findWithoutFail($id);
        /*$inventario = DB::table('inventarios')
            ->join('products', 'products.id', '=', 'inventarios.productos_id')
            ->where('inventarios.id', '=', $id)
            ->get();
            */

        if (empty($inventario)) {
            Flash::error('Inventario not found');

            return redirect(route('inventarios.index'));
        }

        return view('inventarios.edit')->with('inventario', $inventario);
    }

    /**
     * Update the specified inventario in storage.
     *
     * @param  int              $id
     * @param UpdateinventarioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateinventarioRequest $request)
    {
        $inventario = $this->inventarioRepository->findWithoutFail($id);

        if (empty($inventario)) {
            Flash::error('Inventario not found');

            return redirect(route('inventarios.index'));
        }

        $inventario = $this->inventarioRepository->update($request->all(), $id);

        Flash::success('Inventario updated successfully.');

        return redirect(route('inventarios.index'));
    }

    /**
     * Remove the specified inventario from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inventario = $this->inventarioRepository->findWithoutFail($id);

        if (empty($inventario)) {
            Flash::error('Inventario not found');

            return redirect(route('inventarios.index'));
        }

        $this->inventarioRepository->delete($id);

        Flash::success('Inventario deleted successfully.');

        return redirect(route('inventarios.index'));
    }

   /* public function infoInventario($id)
    {
        //$producto = $this->inventarioRepository->findWithoutFail($id);
        $fill_productos = \DB::table('inventarios')->where('productos_id', $id)->get();
         return Response::json(['success'=>true, 'info'=>$fill_productos]);
    }*/

    public function upload($id){

        //$producto = Product::findOrFail($id);
        $producto = DB::table('products')
            ->join('inventarios', 'inventarios.productos_id', '=', 'products.id')
            ->where('inventarios.id', '=', $id)
            ->get();
        return view('inventarios.upload')->with('producto', $producto)->with('estatus', '1');

    }

    public function download($id){

        //$producto = Product::findOrFail($id);
        $producto = DB::table('products')
            ->join('inventarios', 'inventarios.productos_id', '=', 'products.id')
            ->where('inventarios.id', '=', $id)
            ->get();
        return view('inventarios.upload')->with('producto', $producto)->with('estatus', '2');

    }

    public function upload_post(Request $request){

        $producto = $this->inventarioRepository->findWithoutFail($request->input('id'));

        /*Cantidad de productos en inventario*/
        $cantidad_inventario = $producto->cantidad;


        if($request->input('estatus') == "1"){
        $cantidad_total = $request->input('cantidad') + $cantidad_inventario;
         
        }
        else
        {
            if($cantidad_inventario>= $request->input('cantidad')){
            $cantidad_total = $cantidad_inventario - $request->input('cantidad');
            }
            else{
                Flash::warning('Cantidad en inventario es menor a cantidad de descarga.');
                return redirect(route('inventarios.index'));
            }
        }
        $movimiento = movimiento_inventario::create($request->all());
        $inventario = new inventario;
        inventario::where('productos_id', '=', $request->input('productos_id'))
            ->update([
                'cantidad' => $cantidad_total,
            ]);

        if($request->input('estatus') == "1") {
            Flash::success('Carga de producto en inventario exitoso.');
        }
        else{
            Flash::warning('Descarga de producto en inventario exitoso.');

        }
        return redirect(route('inventarios.index'));

    }



}
