<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
/*modelos*/

use App\Models\clientes;
use App\Models\notas_entrega;
use App\Models\items_notas_entregas;

use App\Models\notas_debito;
use App\Models\items_notas_debito;
use App\Models\notas_credito;
use App\Models\items_notas_credito;

use App\Models\factura;
use App\Models\items_factura;

use App\Models\configuracion;



class reporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	$notas_entrega= DB::table('notas_entregas')
    	->join('clientes', 'clientes.id', '=', 'notas_entregas.empresa_nombre')
    	->whereNull('notas_entregas.deleted_at')
      ->select('notas_entregas.*', 'nombre_razon_social')
      ->paginate(60);

    	$clientes= clientes::all(['id', 'nombre_razon_social']);



      $facturas = DB::table('facturas')
    	->join('clientes', 'clientes.id', '=', 'facturas.clientes_id')
          ->whereNull('facturas.deleted_at')
      ->select('facturas.*', 'nombre_razon_social', 'nota_codigo')
    	->paginate(60);
      dd($facturas);
      //$facturas->setPath('reportes%123tab_4');


        return view ('reportes.index')->with('clientes', $clientes)->with('notas_entrega', $notas_entrega)->with('notas_debito', $notas_debito)->with('notas_credito', $notas_credito)->with('facturas', $facturas);
    }


}
