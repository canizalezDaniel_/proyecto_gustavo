<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = DB::table('products')
            ->join('inventarios', 'inventarios.productos_id', '=', 'products.id')
            ->whereNull('products.deleted_at')
            ->select('products.*', 'cantidad', 'fecha_entrada')
            ->paginate(4);
            //dd($productos);

        $facturados= DB::table('facturas')
            ->join('items_facturas', 'items_facturas.facturas_id','=', 'facturas.id')
            ->join('products','products.id', '=', 'items_facturas.productos_id')
            ->groupBy('codigo')
            ->select(DB::raw("count(*) as cantidad, codigo, products.descripcion, facturas.created_at as 'factura_fecha'"))

            ->get();

        $vendedores = DB::table('facturas')
           
            ->join('vendedors','vendedors.id', '=', 'facturas.vendedors_id')
            ->groupBy('vendedors.ci', 'vendedors.nombre', DB::raw('YEAR(facturas.created_at)'),
                DB::raw('MONTH(facturas.created_at)'))
            ->orderBy(DB::raw('YEAR(facturas.created_at)'), DB::raw('MONTH(facturas.created_at)'))
            ->select(DB::raw("count(*) as cantidad, vendedors.ci, vendedors.nombre, YEAR(facturas.created_at) AS anno,
            MONTH(facturas.created_at) AS mes"))
            ->get();

            


        return view('home')->with('productos', $productos)->with('facturados', $facturados)->with('vendedores', $vendedores);

    }

    
        
}
