<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatevendedorRequest;
use App\Http\Requests\UpdatevendedorRequest;
use App\Repositories\vendedorRepository;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class vendedorController extends BaseController
{
    /** @var  vendedorRepository */
    private $vendedorRepository;

    public function __construct(vendedorRepository $vendedorRepo)
    {
        $this->vendedorRepository = $vendedorRepo;
    }

    /**
     * Display a listing of the vendedor.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->vendedorRepository->pushCriteria(new RequestCriteria($request));
        $vendedors = $this->vendedorRepository->all();

        return view('vendedors.index')
            ->with('vendedors', $vendedors);
    }

    /**
     * Show the form for creating a new vendedor.
     *
     * @return Response
     */
    public function create()
    {
        return view('vendedors.create');
    }

    /**
     * Store a newly created vendedor in storage.
     *
     * @param CreatevendedorRequest $request
     *
     * @return Response
     */
    public function store(CreatevendedorRequest $request)
    {
        $input = $request->all();

        $vendedor = $this->vendedorRepository->create($input);

        Flash::success('Vendedor registrado con exito.');

        return redirect(route('vendedors.index'));
    }

    /**
     * Display the specified vendedor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vendedor = $this->vendedorRepository->findWithoutFail($id);

        if (empty($vendedor)) {
            Flash::error('Vendedor no encontrado');

            return redirect(route('vendedors.index'));
        }

        return view('vendedors.show')->with('vendedor', $vendedor);
    }

    /**
     * Show the form for editing the specified vendedor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vendedor = $this->vendedorRepository->findWithoutFail($id);

        if (empty($vendedor)) {
            Flash::error('Vendedor no encontrado');

            return redirect(route('vendedors.index'));
        }

        return view('vendedors.edit')->with('vendedor', $vendedor);
    }

    /**
     * Update the specified vendedor in storage.
     *
     * @param  int              $id
     * @param UpdatevendedorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatevendedorRequest $request)
    {
        $vendedor = $this->vendedorRepository->findWithoutFail($id);

        if (empty($vendedor)) {
            Flash::error('Vendedor no encontrado');

            return redirect(route('vendedors.index'));
        }

        $vendedor = $this->vendedorRepository->update($request->all(), $id);

        Flash::success('Vendedor modificado con exito.');

        return redirect(route('vendedors.index'));
    }

    /**
     * Remove the specified vendedor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vendedor = $this->vendedorRepository->findWithoutFail($id);

        if (empty($vendedor)) {
            Flash::error('Vendedor no encontrado');

            return redirect(route('vendedors.index'));
        }

        $this->vendedorRepository->delete($id);

        Flash::success('Vendedor fue eliminado con exito.');

        return redirect(route('vendedors.index'));
    }
}
