<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Models\notas_entrega;
use App\Models\notas_debito;
use App\Models\items_notas_debito;
use App\Models\items_notas_entregas;
use App\Models\items_notas_credito;
use App\Models\factura;
use App\Models\items_factura;
use App\Models\configuracion;
use Illuminate\Support\Facades\DB;
use PDF;
class pdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notaEntrega($id_nota)
    {


        $nota_entrega = DB::table('notas_entregas')
            ->join('clientes', 'clientes.id', '=', 'notas_entregas.empresa_nombre')
            ->where('notas_entregas.id', $id_nota)->get();

        $items_notas_entregas = DB::table('items_notas_entregas')
            ->join('products', 'products.id', '=', 'items_notas_entregas.productos_id')
            ->where('notas_entrega_id', $id_nota)->get();

        $pdf = \App::make('dompdf.wrapper');
        libxml_use_internal_errors(true);
        $pdf = PDF::loadView('pdf.nota', compact('nota_entrega', 'items_notas_entregas'));
        return $pdf->stream('invoice.pdf');





    }

    public function factura($id_factura){

        $factura = DB::table('facturas')
            ->join('clientes', 'clientes.id', '=', 'facturas.clientes_id')
            ->join('vendedors', 'vendedors.id', '=', 'facturas.vendedors_id')
            ->where('facturas.id', $id_factura)->get();

        //$items_factura = items_factura::where('facturas_id', $id_factura)->get();

        $items_factura  = DB::table('items_facturas')
            ->join('products', 'products.id', '=', 'items_facturas.productos_id')
            ->where('items_facturas.facturas_id', $id_factura)->get();
        $configuracion= configuracion::orderBy('id', 'desc')->first();
        $iva_bs=$configuracion->iva_bs;


        $pdf = \App::make('dompdf.wrapper');
        libxml_use_internal_errors(true);
        $pdf = PDF::loadView('pdf.factura', compact('factura', 'items_factura', 'iva_bs'));
        return $pdf->stream('invoice.pdf');




    }






}
