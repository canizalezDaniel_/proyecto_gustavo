<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatefacturaRequest;
use App\Http\Requests\UpdatefacturaRequest;
use App\Models\inventario;
use App\Repositories\facturaRepository;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\clientes;
use App\Models\vendedor;
use App\Models\Product;
use App\Models\factura;
use App\Models\items_factura;
use App\Models\notas_entrega;
use App\Models\configuracion;
use Illuminate\Support\Facades\Input;
use DB;
use Validator;

class facturaController extends BaseController
{
    /** @var  facturaRepository */
    private $facturaRepository;

    public function __construct(facturaRepository $facturaRepo)
    {
        $this->facturaRepository = $facturaRepo;
    }

    /**
     * Display a listing of the factura.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->facturaRepository->pushCriteria(new RequestCriteria($request));
        $facturas = $this->facturaRepository->all();

        $clientes = clientes::all(['id', 'nombre_razon_social']);



        return view('facturas.index')
            ->with('facturas', $facturas)->with('clientes', $clientes);
    }

    /**
     * Show the form for creating a new factura.
     *
     * @return m_responsekeys(conn, identifier)
     */
    public function create()
    {
        $clientes = clientes::all(['id', 'nombre_razon_social']);
        $vendedores = vendedor::all(['id', 'nombre']);

        $productos = DB::table('products')
            ->join('inventarios', 'inventarios.productos_id', '=', 'products.id')
            ->select('products.id as idP','inventarios.id','products.codigo', 'products.descripcion', 'inventarios.fecha_entrada', 'inventarios.cantidad', 'inventarios.precio')
            ->get();

        //$productos = Product::all(['id', 'codigo', 'descripcion']);
        $iva_usd = DB::table('configuracions')->first();
        $iva_usd_porcentaje= $iva_usd->iva_usd/100;
        $iva_bs_porcentaje= $iva_usd->iva_bs/100;
        $tasa_cambio = $iva_usd->tasa_usd;
        $codigo =$iva_usd->factura_codigo;
        $longitud=6;
        $factura_codigo=str_pad($codigo+1, $longitud, '0', STR_PAD_LEFT);


        $notas_entregas= notas_entrega::all(['id','nota_codigo']);
        return view('facturas.create')->with('clientes', $clientes)->with('iva', $iva_usd_porcentaje)->with('iva_bs_porcentaje', $iva_bs_porcentaje)->with('tasa_cambio', $tasa_cambio)->with('vendedores', $vendedores)->with('productos', $productos)
            ->with('notas_entregas', $notas_entregas)->with('factura_codigo', $factura_codigo);
    }

    /**
     * Store a newly created factura in storage.
     *
     * @param CreatefacturaRequest $request
     *
     * @return Response
     */
    public function store()
    {

        //dd(Input::all());
        /*Validaciones comienza*/
        //validacion modificacion del campo validado

        $nombre_campo = array(

            'clientes_id' => 'Cliente',
            'fecha_emision'  => 'Fecha de emisión',
            'factura_codigo'=> 'Codigo de factura',
            // 'nota_codigo' => 'Codigo de nota de entrega',
            'condicion_pago'=> 'Codigo de pago',


        );

        $reglas = array(

            'clientes_id' => 'required',
            'fecha_emision' => 'date',
            'factura_codigo' => 'required',
            'condicion_pago' => 'required'


        );

        //personalizamos los mensajes de error para nuestro formulario
        $mensaje = array(
            'required' => 'El campo :attribute es requerido.',
            'alpha'   => 'Solo se permiten caracteres en el campo :attribute .',
            'date'    => 'Campo :attribute en formato YYYY-MM-DD'

        );

        //variable que realiza la validacion del submit
        $validator = Validator::make(Input::all(), $reglas, $mensaje);
        //asignamos los atribulos o los nombres dados a los campos
        $validator->setAttributeNames($nombre_campo);

        if ($validator->fails())
        {
            // return redirect('notasEntregas/create')->withErrors($validator);
            return redirect()->back()->withErrors($validator->errors());
        }

        /*Validacion termina*/

        $inputs = Input::all();
        $cabecera_inputs = Input::only('lugar_emision','fecha_emision','vendedors_id','clientes_id', 'factura_codigo', 'condicion_pago', 'nota_codigo', 'dias', 'forma_pago', 'condicion_pago', 'credito_condicion_dias','cheque_numero', 'banco_cuenta', 'vencimiento', 'forma_pago', 'credito_dias','total_base', 'ajustes', 'iva_bs', 'total_bs');
        $factura = factura::where('factura_codigo', Input::get('factura_codigo'))

            ->whereNull('deleted_at')
            ->exists();

        $configuracions = configuracion::find(1);

        $configuracions->factura_codigo = $configuracions->factura_codigo+1;

        $configuracions->save();


        if ($factura) {

            return back()->with('mensaje','<strong> Lo sentimos, </strong>el código '.Input::get('factura_codigo').' de factura ya existe en nuestros registros.');

        }
        $factura = $this->facturaRepository->create($cabecera_inputs);
        $LastInsertId = $factura->id;

        //OBTENER ULTIMO ID DE PUBLICACION
        $factura_id = factura::find($LastInsertId);



        foreach($inputs['cantidad'] as $index => $value)
        {

            /*Control sobre inventario*/

            //comprobar ID de inventario

            inventario::where('productos_id', '=', $inputs['productos_id'][$index])

                ->update([
                    'cantidad' => $inputs['cantidad_producto'][$index] - $inputs['cantidad'][$index],
                ]);


            $items_factura = new items_factura;
            $items_factura->facturas()->associate($factura);
            $items_factura->productos_id = $inputs['productos_id'][$index];
            $items_factura->cantidad = $inputs['cantidad'][$index];
            $items_factura->precio_unitario_bs = $inputs['precio_unitario_bs'][$index];
            $items_factura->total_item_bs = $inputs['total_item_bs'][$index];
            $items_factura->save();
        }
        //Flash::success('Factura saved successfully.');
        Flash::success('<h4><i class="icon fa fa-check"> </i> <strong>FACTURA Nº </strong>'. Input::get('factura_codigo').' </h4> Su factura ha sido creada satisfactoriamente en el sistema <br> <br> <a href=impresoraPdffactura/'.$factura_id->id.' class="btn btn-warning" target="_blank" style="text-decoration: none"> <i class="fa fa-print"> </i> <span> Imprimir </span> </a>');

       // return view('home');
        return redirect('home');
    }

    /**
     * Display the specified factura.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $factura = $this->facturaRepository->findWithoutFail($id);

        if (empty($factura)) {
            Flash::error('Factura no encontrada');

            return redirect(route('facturas.index'));
        }

        return view('facturas.show')->with('factura', $factura);
    }

    /**
     * Show the form for editing the specified factura.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $factura = $this->facturaRepository->findWithoutFail($id);

        $factura_codigo=$factura->factura_codigo;
        $items_facturas = items_factura::where('facturas_id', $id)->get();

        $clientes = DB::table('clientes')->get();

        $cliente_factura = DB::table('facturas')
            ->join('clientes', 'clientes.id', '=', 'facturas.empresa_nombre')
            ->where('facturas.id', $id)->get();

        $iva_usd = DB::table('configuracions')->first();
        $iva_usd_porcentaje= $iva_usd->iva_usd/100;
        $iva_bs_porcentaje= $iva_usd->iva_bs/100;
        $tasa_cambio = $iva_usd->tasa_usd;
        $codigo =$iva_usd->factura_codigo;


        return view('facturas.edit')->with('cliente_factura', $cliente_factura)->with('factura_codigo', $factura_codigo)->with('factura', $factura)->with('items_facturas', $items_facturas)->with('iva', $iva_usd_porcentaje)->with('iva_bs_porcentaje', $iva_bs_porcentaje)->with('tasa_cambio', $tasa_cambio)->with('clientes', $clientes);
    }

    /**
     * Update the specified factura in storage.
     *
     * @param  int              $id
     * @param UpdatefacturaRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $factura = $this->facturaRepository->findWithoutFail($id);

        $inputs = Input::only('id','renglon','descripcion');


        $cabecera_inputs = Input::only('lugar_emision','empresa_nombre', 'empresa_direccion', 'fecha_emision', 'ci_rif', 'telefono', 'factura_codigo', 'condicion_pago', 'nota_codigo','observaciones', 'dias', 'forma_pago', 'condicion_pago', 'credito_condicion_dias','cheque_numero', 'banco_cuenta', 'vencimiento', 'forma_pago', 'credito_dias', 'sub_total_usd', 'iva_usd', 'total_usd', 'total_base', 'ajustes', 'total_base_ajustes', 'iva_bs', 'total_bs', 'observaciones');
        $configuracions = configuracion::find(1);
        $configuracions->factura_codigo = Input::get('factura_codigo');
        $configuracions->save();
        $factura = $this->facturaRepository->update($cabecera_inputs, $id);

        $LastInsertId = $factura->id;

        //OBTENER ULTIMO ID DE FACTURA
        $factura_id = factura::find($LastInsertId);


        for ($i = 0; $i < count($inputs['id']); $i++)
        {

            items_factura::where('id', '=', $inputs['id'][$i])
                ->update([
                    'renglon' =>  $inputs['renglon'][$i],
                    'descripcion' => $inputs['descripcion'][$i]

                ]);



        }






        Flash::success('<h4><i class="icon fa fa-check"> </i> <strong>FACTURA Nº </strong>'. Input::get('factura_codigo').' </h4> Su factura ha sido modificada satisfactoriamente en el sistema <br> <br> <a href=impresoraPdffactura/'.$factura_id->id.' class="btn btn-warning" target="_blank" style="text-decoration: none"> <i class="fa fa-print"> </i> <span> Imprimir </span> </a>');

        return redirect('home');


    }

    /**
     * Remove the specified factura from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $factura = $this->facturaRepository->findWithoutFail($id);

        if (empty($factura)) {
            Flash::error('Factura no encontrada');

            return redirect(route('facturas.index'));
        }

        $this->facturaRepository->delete($id);

        Flash::success('Factura eliminada con exito');

        return redirect(route('reporteFactura'));
    }

    public function anular($id){

        $factura = factura::find($id);
        $factura->estatus = '0';
        $factura->save();

        Flash::success('Factura anulada con exito');

        return redirect(route('reporteFactura'));

    }

    public function reportes(){

        $facturas = DB::table('facturas')
            ->join('clientes', 'clientes.id', '=', 'facturas.clientes_id')
            ->whereNull('facturas.deleted_at')
            ->select('facturas.*', 'nombre_razon_social', 'clientes.ci_rif','nota_codigo')
            ->paginate(15);

        $clientes= clientes::all(['id', 'nombre_razon_social']);

        return view ('facturas.reporte')->with('clientes', $clientes)->with('facturas', $facturas);

    }

    public function infoInventario($id)
    {
        //$producto = ->findWithoutFail($id);
        $fill_productos = \DB::table('inventarios')->where('productos_id', $id)->get();
        return Response::json(['success'=>true, 'info'=>$fill_productos]);
    }

}
