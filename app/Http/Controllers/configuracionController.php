<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateconfiguracionRequest;
use App\Http\Requests\UpdateconfiguracionRequest;
use App\Repositories\configuracionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class configuracionController extends BaseController
{
    /** @var  configuracionRepository */
    private $configuracionRepository;

    public function __construct(configuracionRepository $configuracionRepo)
    {
        $this->configuracionRepository = $configuracionRepo;
    }

    /**
     * Display a listing of the configuracion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->configuracionRepository->pushCriteria(new RequestCriteria($request));
        $configuracions = $this->configuracionRepository->all();
        return view('configuracions.index')
            ->with('configuracions', $configuracions);
    }

    /**
     * Show the form for creating a new configuracion.
     *
     * @return Response
     */
    public function create()
    {
        return view('configuracions.create');
    }

    /**
     * Store a newly created configuracion in storage.
     *
     * @param CreateconfiguracionRequest $request
     *
     * @return Response
     */
    public function store(CreateconfiguracionRequest $request)
    {
        $input = $request->all();

        $configuracion = $this->configuracionRepository->create($input);

        Flash::success('Configuración guardada con exito.');

        return redirect(route('configuracions.index'));
    }

    /**
     * Display the specified configuracion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $configuracion = $this->configuracionRepository->findWithoutFail($id);

        if (empty($configuracion)) {
            Flash::error('Configuracion not found');

            return redirect(route('configuracions.index'));
        }

        return view('configuracions.show')->with('configuracion', $configuracion);
    }

    /**
     * Show the form for editing the specified configuracion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $configuracion = $this->configuracionRepository->findWithoutFail($id);

        if (empty($configuracion)) {
            Flash::error('Configuracion not found');

            return redirect(route('configuracions.index'));
        }

        return view('configuracions.edit')->with('configuracion', $configuracion);
    }

    /**
     * Update the specified configuracion in storage.
     *
     * @param  int              $id
     * @param UpdateconfiguracionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateconfiguracionRequest $request)
    {
        $configuracion = $this->configuracionRepository->findWithoutFail($id);

        if (empty($configuracion)) {
            Flash::error('Configuracion not found');

            return redirect(route('configuracions.index'));
        }

        $configuracion = $this->configuracionRepository->update($request->all(), $id);

        Flash::success('Configuración realizada con exito.');

        return redirect(route('configuracions.index'));
    }

    /**
     * Remove the specified configuracion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $configuracion = $this->configuracionRepository->findWithoutFail($id);

        if (empty($configuracion)) {
            Flash::error('Configuracion not found');

            return redirect(route('configuracions.index'));
        }

        $this->configuracionRepository->delete($id);

        Flash::success('Configuracion deleted successfully.');

        return redirect(route('configuracions.index'));
    }
}
