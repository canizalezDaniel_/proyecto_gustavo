<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createnotas_entregaRequest;
use App\Http\Requests\Updatenotas_entregaRequest;
use App\Models\Product;
use App\Repositories\notas_entregaRepository;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\clientes;
use App\Models\items_notas_entregas;
use App\Models\notas_entrega;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;

class notas_entregaController extends BaseController
{
  /** @var  notas_entregaRepository */
  private $notasEntregaRepository;

  public function __construct(notas_entregaRepository $notasEntregaRepo)
  {
    $this->notasEntregaRepository = $notasEntregaRepo;
  }

  /**
  * Display a listing of the notas_entrega.
  *
  * @param Request $request
  * @return Response
  */
  public function index(Request $request)
  {
    $this->notasEntregaRepository->pushCriteria(new RequestCriteria($request));
    $notasEntregas = $this->notasEntregaRepository->all();





    return view('notas_entregas.index')
    ->with('notasEntregas', $notasEntregas);
  }

  /**
  * Show the form for creating a new notas_entrega.
  *
  * @return Response
  */
  public function create()
  {

    // $notasEntrega = \DB::table('notas_entregas')->first();
    $ultima_nota=  notas_entrega::orderBy('id', 'desc')->first();
    if($ultima_nota==null){
      $codigo_nota=1;
    }else{
      $codigo_nota=$ultima_nota->nota_codigo+1;
    }

    $clientes = clientes::all(['id', 'nombre_razon_social']);
    $productos = Product::all(['id', 'codigo', 'descripcion']);

      return view('notas_entregas.create')->with('clientes', $clientes)->with('codigo_nota', $codigo_nota)->with('productos', $productos);
  }

  /**
  * Store a newly created notas_entrega in storage.
  *
  * @param Createnotas_entregaRequest $request
  *
  * @return Response
  */
  public function store()
  {

    /*Validaciones comienza*/
    //validacion modificacion del campo validado

    $nombre_campo = array(
      'lugar_emision' => 'Lugar de emisión',
      'empresa_nombre' => 'Empresa',
      'fecha_emision'  => 'Fecha de emisión',
      'telefono' => 'Telefono de empresa',
      'ci_rif'  => 'RIF/CI Empresa',
      'empresa_direccion' => 'Domicilió fiscal'


    );

    $reglas = array(
      'lugar_emision'  => 'required',
      'empresa_nombre' => 'required',
      'fecha_emision' => 'date'
    );

    //personalizamos los mensajes de error para nuestro formulario
    $mensaje = array(
      'required' => 'El campo :attribute es requerido.',
      'alpha'   => 'Solo se permiten caracteres en el campo :attribute .',
      'date'    => 'Campo :attribute en formato YYYY-MM-DD'
    );

    //variable que realiza la validacion del submit
    $validator = Validator::make(Input::all(), $reglas, $mensaje);
    //asignamos los atribulos o los nombres dados a los campos
    $validator->setAttributeNames($nombre_campo);

    if ($validator->fails())
    {
      // return redirect('notasEntregas/create')->withErrors($validator);
      return redirect()->back()->withErrors($validator->errors());
    }

    /*Validacion termina*/

      $inputs = Input::all();
      $cabecera_inputs = Input::only('lugar_emision','empresa_nombre', 'empresa_direccion', 'fecha_emision', 'ci_rif', 'telefono', 'nota_codigo', 'recibido_por', 'nombre_transportista', 'cedula_transportista', 'empresa_transportista', 'vehiculo_empresa', 'marca_vehiculo', 'placa_vehiculo', 'observaciones_nota');
      $notasEntrega = $this->notasEntregaRepository->create($cabecera_inputs);

      $LastInsertId = $notasEntrega->id;

      //OBTENER ULTIMO ID DE PUBLICACION
      $nota_entrega_id = notas_entrega::find($LastInsertId);



      foreach($inputs['item'] as $index => $value) {

        $items_notas_entregas = new items_notas_entregas;

        $items_notas_entregas->notas_entrega()->associate($notasEntrega);
        $items_notas_entregas->item = $inputs['item'][$index];
        $items_notas_entregas->productos_id = $inputs['productos_id'][$index];
        //$items_notas_entregas->observaciones = $inputs['observaciones'][$index];

        //$items_notas_entregas->unidad= $inputs['unidad'][$index];

        $items_notas_entregas->cantidad = $inputs['cantidad'][$index];
        //$items_notas_entregas->cantidad_entregada = $inputs['cantidad_entregada'][$index];
        $items_notas_entregas->save();
      }



      Flash::success('<h4><i class="icon fa fa-check"> </i> <strong>NOTA DE ENTREGA Nº </strong>'. Input::get('nota_codigo').' </h4> Su nota de entrega fue creada satisfactoriamente en el sistema <br> <br> <a href=impresoraPdf/'.$nota_entrega_id->id.' class="btn btn-warning" target="_blank" style="text-decoration: none"> <i class="fa fa-print"> </i> <span> Imprimir </span> </a>');
      return redirect('home');

  }

  /**
  * Display the specified notas_entrega.
  *
  * @param  int $id
  *
  * @return Response
  */
  public function show($id)
  {
    $notasEntrega = $this->notasEntregaRepository->findWithoutFail($id);

    if (empty($notasEntrega)) {
      Flash::error('Notas Entrega not found');

      return redirect(route('notasEntregas.index'));
    }

    return view('notas_entregas.show')->with('notasEntrega', $notasEntrega);
  }

  /**
  * Show the form for editing the specified notas_entrega.
  *
  * @param  int $id
  *
  * @return Response
  */
  public function edit($id)
  {
    $notasEntrega = $this->notasEntregaRepository->findWithoutFail($id);
    $codigo_nota=$notasEntrega->nota_codigo;
    $items_notas_entregas = items_notas_entregas::where('notas_entrega_id', $id)->get();
    
    $cliente_nota_entrega = DB::table('notas_entregas')
    ->join('clientes', 'clientes.id', '=', 'notas_entregas.empresa_nombre')
    ->where('notas_entregas.id', $id)->get();
    $productos = Product::all(['id', 'codigo', 'descripcion']);

    
    return view('notas_entregas.edit')->with('cliente_nota_entrega', $cliente_nota_entrega)->with('codigo_nota', $codigo_nota)->with('notasEntrega', $notasEntrega)->with('items_notas_entregas', $items_notas_entregas)->with('productos', $productos);
  }

  /**
  * Update the specified notas_entrega in storage.
  *
  * @param  int              $id
  * @param Updatenotas_entregaRequest $request
  *
  * @return Response
  */
  public function update($id, Request $request)
  {
    $notasEntrega = $this->notasEntregaRepository->findWithoutFail($id);

    
   
    /*Editar nota de entrega*/

    $inputs = Input::only('id','item','descripcion', 'observaciones', 'unidad','cantidad', 'cantidad_entregada');


      $cabecera_inputs = Input::only('lugar_emision','empresa_nombre', 'empresa_direccion', 'fecha_emision', 'ci_rif', 'telefono', 'nota_codigo', 'recibido_por', 'nombre_transportista', 'cedula_transportista', 'empresa_transportista', 'vehiculo_empresa', 'marca_vehiculo', 'placa_vehiculo', 'observaciones_nota');
      $notasEntrega = $this->notasEntregaRepository->update($cabecera_inputs, $id);

      $LastInsertId = $notasEntrega->id;

      //OBTENER ULTIMO ID DE PUBLICACION
      $nota_entrega_id = notas_entrega::find($LastInsertId);

         for ($i = 0; $i < count($inputs['item']); $i++) 
        {
                  
               items_notas_entregas::where('id', '=', $inputs['id'][$i])
          ->update([
              'item' =>  $inputs['item'][$i],
              'producto' => $inputs['producto'][$i],
              'cantidad' => $inputs['cantidad'][$i],
                    
          ]);

         
                   
          }

      

       
     

      Flash::success('<h4><i class="icon fa fa-check"> </i> <strong>NOTA DE ENTREGA Nº </strong>'. Input::get('nota_codigo').' </h4> Su nota de entrega fue modificada satisfactoriamente en el sistema <br> <br> <a href=impresoraPdf/'.$nota_entrega_id->id.' class="btn btn-warning" target="_blank" style="text-decoration: none"> <i class="fa fa-print"> </i> <span> Imprimir </span> </a>');
      return redirect('home');


  }

  /**
  * Remove the specified notas_entrega from storage.
  *
  * @param  int $id
  *
  * @return Response
  */
  public function destroy($id)
  {


    $notasEntrega = $this->notasEntregaRepository->findWithoutFail($id);

    if (empty($notasEntrega)) {
      Flash::error('Nota de entrega no encontrada');

      return redirect(route('reportes.index'));
    }

    $this->notasEntregaRepository->delete($id);

    Flash::success('Nota de entrega eliminada con exito');

    return redirect(route('reporteNotae'));
  }


  public function anular($id){

    $notasEntrega = notas_entrega::find($id);
    $notasEntrega->estatus = '0';
    $notasEntrega->save();

    Flash::success('Nota de entrega anulada con exito');

    return redirect(route('reporteNotae'));

  }


  public function reportes(){
    
      $notas_entrega= DB::table('notas_entregas')
      ->join('clientes', 'clientes.id', '=', 'notas_entregas.empresa_nombre')
      ->whereNull('notas_entregas.deleted_at')
      ->select('notas_entregas.*', 'nombre_razon_social')
      ->paginate(15);

      $clientes= clientes::all(['id', 'nombre_razon_social']);

      return view ('notas_entregas.reporte')->with('clientes', $clientes)->with('notas_entrega', $notas_entrega);

  }
}
