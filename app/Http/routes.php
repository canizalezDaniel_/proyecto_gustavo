<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('error', function(){
    abort(500);
});
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::auth();

Route::get('home', 'HomeController@index');
Route::get('subnotas', function(){

	return view('notas');
});

/*Controlador nota de entrega*/
Route::resource('notasEntregas', 'notas_entregaController');
Route::get('anularNotae/{id}', 'notas_entregaController@anular');
Route::get('reporteNotae', 'notas_entregaController@reportes')->name('reporteNotae');





Route::resource('facturas', 'facturaController');
Route::get('anularFactura/{id}', 'facturaController@anular');
Route::get('reporteFactura', 'facturaController@reportes')->name('reporteFactura');


Route::resource('clientes', 'clientesController');
Route::resource('users', 'UserController');

Route::resource('vendedors', 'vendedorController');
Route::resource('products', 'ProductController');
Route::resource('inventarios', 'inventarioController');

/*Carga y descarga de inventario*/

Route::get('inventarioCarga/{id}', 'inventarioController@upload')->name('inventarios.upload');
Route::get('inventarioDescarga/{id}', 'inventarioController@download')->name('inventarios.download');
Route::post('inventarioCargaPost', 'inventarioController@upload_post')->name('inventarios.upload_post');
Route::resource('configuracions', 'configuracionController');



/*Controlador para seleccion de empresa*/
Route::get('notasEntregas/infoCliente/{id}', 'clientesController@infoCliente');
Route::get('facturas/infoCliente/{id}', 'clientesController@infoCliente');

/*Controlador para devolver informacion de inventario*/
Route::get('facturas/infoInventario/{id}', 'facturaController@infoInventario');


Route::get('impresoraPdf/{id_nota}', 'pdfController@notaEntrega');
Route::get('impresoraPdffactura/{id_factura}', 'pdfController@factura');
