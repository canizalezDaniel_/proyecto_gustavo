<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotasEntregasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas_entregas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lugar_emision');
            $table->string('empresa_nombre');
            $table->string('empresa_direccion');
            $table->date('fecha_emision');
            $table->string('ci_rif');
            $table->string('telefono');
            $table->string('nota_codigo');
            $table->string('recibido_por');
            $table->string('nombre_transportista');
            $table->string('cedula_transportista');
            $table->string('empresa_transportista');
            $table->string('vehiculo_empresa');
            $table->string('marca_vehiculo');
            $table->string('placa_vehiculo');
            $table->string('observaciones_nota');
            $table->integer('estatus')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notas_entregas');
    }
}
