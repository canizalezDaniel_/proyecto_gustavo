<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsNotasFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad');
            $table->text('descripcion');
            $table->string('precio_unitario_bs');
            $table->string('total_item_bs');
            $table->integer('facturas_id')->unsigned();
            $table->foreign('facturas_id')->references('id')->on('facturas');

            $table->integer('productos_id')->unsigned();
            $table->foreign('productos_id')->references('id')->on('products');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items_facturas');
    }
}
