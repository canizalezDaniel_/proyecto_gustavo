<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientoInventarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('movimiento_inventario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('productos_id')->unsigned();
            $table->foreign('productos_id')->references('id')->on('products');


            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');

            /*$table->integer('motivo_carga_id')->unsigned();
            $table->foreign('motivo_carga_id')->references('id')->on('motivos_carga');*/

            $table->string('estatus', 1); //1 carga 2 descarga
            $table->integer('cantidad');
            $table->string('motivo');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movimiento_inventario');

    }
}
