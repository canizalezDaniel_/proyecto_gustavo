<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsNotasCreditosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_notas_creditos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad');
            $table->text('descripcion');
            $table->string('total_item_usd');
            $table->string('total_item_bs');

            $table->integer('notas_creditos_id')->unsigned();
            $table->foreign('notas_creditos_id')->references('id')->on('notas_creditos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items_notas_creditos');
    }
}
