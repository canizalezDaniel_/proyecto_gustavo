<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatefacturasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lugar_emision');
            //$table->string('empresa_nombre');
            //$table->string('empresa_direccion');
            //$table->string('ci_rif');
            $table->date('fecha_emision');
            //$table->string('telefono');
            $table->string('factura_codigo');
            $table->string('nota_codigo')->nullable();
            $table->enum('condicion_pago', ['Contado', 'Credito'])->default('Contado');
            $table->integer('credito_condicion_dias')->nullable();
            $table->string('cheque_numero')->nullable();
            $table->string('banco_cuenta')->nullable();
            $table->date('vencimiento');
            $table->string('forma_pago')->nullable();
            $table->integer('credito_dias')->nullable();
            $table->integer('estatus')->default('1');
            $table->string('total_base');
            $table->string('ajustes');
            //$table->string('total_base_ajustes');
            $table->string('iva_bs');
            $table->string('total_bs');
            //$table->text('observaciones');


            $table->integer('vendedors_id')->unsigned();
            $table->foreign('vendedors_id')->references('id')->on('vendedors');

            $table->integer('clientes_id')->unsigned();
            $table->foreign('clientes_id')->references('id')->on('clientes');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('facturas');
    }
}
