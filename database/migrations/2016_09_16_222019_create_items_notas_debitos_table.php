<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsNotasDebitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_notas_debitos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad');
            $table->text('descripcion');
            $table->string('total_item_usd');
            $table->string('total_item_bs');

            $table->integer('notas_debitos_id')->unsigned();
            $table->foreign('notas_debitos_id')->references('id')->on('notas_debitos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items_notas_debitos');
    }
}
