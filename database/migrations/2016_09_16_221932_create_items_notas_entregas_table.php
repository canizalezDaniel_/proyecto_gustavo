<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsNotasEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_notas_entregas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('item');
            $table->text('descripcion');
            $table->text('observaciones');
            $table->string('unidad');
            $table->integer('cantidad');
            $table->integer('cantidad_entregada');

            $table->integer('notas_entrega_id')->unsigned();
            $table->foreign('notas_entrega_id')->references('id')->on('notas_entregas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items_notas_entregas');
    }
}
