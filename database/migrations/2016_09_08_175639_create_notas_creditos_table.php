<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotasCreditosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas_creditos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lugar_emision');
            $table->string('empresa_nombre');
            $table->string('empresa_direccion');
            $table->date('fecha_emision');
            $table->string('ci_rif');
            $table->string('telefono');
            $table->string('nota_credito_codigo');
            $table->string('recibido_conforme');
            $table->integer('estatus')->default('1');

            // campos de calculo
            $table->string('iva_usd');
            $table->string('total_usd');
            $table->string('ajustes');
            $table->string('total_base_ajustes');

            $table->string('total_base');
            $table->string('iva_bs');
            $table->string('total_bs');


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notas_creditos');
    }
}
